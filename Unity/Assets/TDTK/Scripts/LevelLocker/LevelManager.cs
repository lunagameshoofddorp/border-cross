﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{


    public class LevelManager : MonoBehaviour
    {
        /*public static void ReLockLevels()
        {
            PlayerPrefs.DeleteKey("gamedata.level");
            PlayerPrefs.DeleteKey("gamedata.levelCompleted");
            //PlayerPrefs.DeleteAll();
        }*/


        public static int GetUnlocked()
        {

            return PlayerPrefs.GetInt("gamedata.level", 0);	// zero based level 0 is unlocked by default!
        }

        public static int GetCompleted()
        {

            return PlayerPrefs.GetInt("gamedata.levelCompleted", -1);	// zero based level 0 is unlocked by default!
        }

        public static bool IsLocked(int level)
        {

            return level > GetUnlocked();
        }

        public static bool IsCompleted(int level)
        {

            return level <= GetCompleted();
        }

        public static void UnlockLevel(int levelUnlocked)
        {

            int current = GetUnlocked();
            if (levelUnlocked > current)
            {

                // only store if actually unlocked greater level
                PlayerPrefs.SetInt("gamedata.level", levelUnlocked);
            }
        }

        public static void CompletedLevel(int levelCompleted)
        {
            int current = GetCompleted();
            if (levelCompleted > current)
            {

                PlayerPrefs.SetInt("gamedata.levelCompleted", levelCompleted);
            }
        }
    }

}