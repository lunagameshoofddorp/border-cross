﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{


    public class LevelUI : MonoBehaviour
    {

        public int levelNumber = 0;
        public GameObject showWhenLocked1;
        public GameObject showWhenLocked2;
        public GameObject showWhenUnLocked;
        public GameObject showWhenCompleted;

        // Use this for initialization
        void Start()
        {

            if (showWhenLocked1) showWhenLocked1.SetActive(LevelManager.IsLocked(levelNumber));
            if (showWhenLocked2) showWhenLocked2.SetActive(LevelManager.IsLocked(levelNumber));
            if (showWhenUnLocked)
            {
                showWhenUnLocked.SetActive(!LevelManager.IsLocked(levelNumber) & !LevelManager.IsCompleted(levelNumber));
                if (!LevelManager.IsLocked(levelNumber) && !LevelManager.IsCompleted(levelNumber))
                {
                    this.gameObject.GetComponent<MainMenuHover>().enabled = true;
                    Debug.Log("Hover Me");
                }
            }
            if (showWhenCompleted) showWhenCompleted.SetActive(!LevelManager.IsLocked(levelNumber) && LevelManager.IsCompleted(levelNumber));

            if (LevelManager.IsLocked(levelNumber))
            {

                UnityEngine.UI.Button b = GetComponent<UnityEngine.UI.Button>();
                if (b != null)
                {
                    b.enabled = false;	// disable button when locked!
                }
            }
        }


    }

}