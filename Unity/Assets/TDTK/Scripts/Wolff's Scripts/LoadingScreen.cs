﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class LoadingScreen : MonoBehaviour
    {

        /*public Texture2D texture480X320;
        public Texture2D texture800X480;
        public Texture2D texture854X480;
        public Texture2D texture960X540;
        public Texture2D texture1024X600;
        public Texture2D texture1280X720;
        public Texture2D texture1280X800;
        public Texture2D texture1920X1080;
        public Texture2D texture2048X1536;
        public Texture2D texture2560X1440;
        public Texture2D texture2560X1600;*/
        public Texture2D texture;
        static LoadingScreen instance;

        void Awake()
 {
     if (instance)
     {
         Destroy(gameObject);
         return;
     }
     instance = this;    
     gameObject.AddComponent<GUITexture>().enabled = false;

    /* CheckScreenHeight();
     CheckScreenWidth();
     if (guiTexture.texture == null)
     {
         Debug.Log("guitexture = null");
         guiTexture.texture = texture1280X800;
     }*/
            
     //else if (Screen.currentResolution.height == 1080 && Screen.currentResolution.height == 1920)
     //       {

     //       }
     guiTexture.texture = texture;
     transform.position = new Vector3(0.5f, 0.5f, 0.0f);
     //DontDestroyOnLoad(this); 
 }
        /*
        public void CheckScreenHeight()
        {
            switch (Screen.currentResolution.height)
            {
                case 320:
                    guiTexture.texture = texture480X320;
                    break;
                case 480:
                    guiTexture.texture = texture800X480;
                    break;
                case 540:
                    guiTexture.texture = texture960X540;
                    break;
                case 600:
                    guiTexture.texture = texture1024X600;
                    break;
                case 720:
                    guiTexture.texture = texture1280X720;
                    break;
                case 800:
                    guiTexture.texture = texture1280X800;
                    break;
                case 1080:
                    guiTexture.texture = texture1920X1080;
                    break;
                case 1536:
                    guiTexture.texture = texture2048X1536;
                    break;
                case 1440:
                    guiTexture.texture = texture2560X1440;
                    break;
                case 1600:
                    guiTexture.texture = texture2560X1600;
                    break;
            }
        }

        public void CheckScreenWidth()
        {
            switch (Screen.currentResolution.width)
            {
                case 480:
                    guiTexture.texture = texture480X320;
                    break;
                case 800:
                    guiTexture.texture = texture800X480;
                    break;
                case 854:
                    guiTexture.texture = texture854X480;
                    break;
                case 960:
                    guiTexture.texture = texture960X540;
                    break;
                case 1024:
                    guiTexture.texture = texture1024X600;
                    break;
                case 1280: //Has two options so let heightCheck decide
                    guiTexture.texture = 
                    break;
                case 1920:
                    guiTexture.texture = texture1920X1080;
                    break;
                case 2048:
                    guiTexture.texture = texture2048X1536;
                    break;
                case 2560://Has two options so let heightCheck decide
                    guiTexture.texture = 
                    break;
            }
        }*/

        public static void Load(int index)
        {
            if (NoInstance()) return;
            instance.guiTexture.enabled = true;
            Application.LoadLevel(index);
            instance.guiTexture.enabled = false;
        }

        public static void Load(string name)
        {
            instance.guiTexture.enabled = true;
            //Debug.Log("Load method entered");
            //if (NoInstance()) return;
            //Debug.Log("No return " + name);
            if (FindObjectOfType<UI>() != null)
            {
                var allUI = FindObjectOfType<UI>();
                allUI.gameObject.SetActive(false);
            }
            Application.LoadLevel(name);
            //DontDestroyOnLoad(instance);
            /*if (Application.isLoadingLevel)
            {
                instance.guiTexture.enabled = true;
            }
            else
            {*/
                //instance.guiTexture.enabled = false;
            //}
        }

        static bool NoInstance()
        {
            if (!instance)
                Debug.LogError("Loading Screen is not existing in scene.");
            return instance;
        }
    }
}