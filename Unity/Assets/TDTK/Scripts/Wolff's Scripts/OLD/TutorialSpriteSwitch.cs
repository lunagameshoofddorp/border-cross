﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class TutorialSpriteSwitch : MonoBehaviour
    {

        public int sprite = 1;
        public Sprite one;
        public Sprite two;
        public Sprite three;
        public Sprite four;
        public Sprite five;
        public Sprite six;
        public Sprite seven;
        public Sprite eight;
        public Sprite nine;
        public Sprite ten;

        public SpriteRenderer spriteRenderer;
        //public Image[] images;

        // Use this for initialization
        void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            //images = gameObject.GetComponentsInChildren<Image>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ChangeScreen()
        {
            //Debug.Log("method entered");
            switch (sprite)
            {       
                case 1:
                    spriteRenderer.sprite = one;
                    break;
                case 2:
                    //Debug.Log("switch case 2 entered");
                    //foreach (Image image in images)
                    //{
                    spriteRenderer.sprite = two;
                        //image.sprite = two;
                        //Debug.Log("Foreach entered");
                    //}
                    break;
                case 3:
                    spriteRenderer.sprite = three;
                    break;
                case 4:
                    spriteRenderer.sprite = four;
                    break;
                case 5:
                    spriteRenderer.sprite = five;
                    break;
                case 6:
                    spriteRenderer.sprite = six;
                    break;
                case 7:
                    spriteRenderer.sprite = seven;
                    break;
                case 8:
                    spriteRenderer.sprite = eight;
                    break;
                case 9:
                    spriteRenderer.sprite = nine;
                    break;
                case 10:
                    spriteRenderer.sprite = ten;
                    break;
            }
        }

    }

}