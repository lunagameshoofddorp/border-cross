﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK
{

    public class NewAI : MonoBehaviour
    {

        public PlatformTD CurrentPlatform;
        public UnitTower currentTower;
        public PathTD priorityPath;
        public int priorityPathNR;
        public string priorityTowerType = "";

        public UnitTower rockTower;
        public UnitTower paperTower;
        public UnitTower scissorTower;

        public PathTD Path1;
        public PathTD Path2;
        public PathTD Path3;

        public static PathTD PathStatic1;
        public static PathTD PathStatic2;
        public static PathTD PathStatic3;

        public GameObject PathPlatform1;
        public GameObject PathPlatform2;
        public GameObject PathPlatform3;

        public List<PlatformTD> Path1List = new List<PlatformTD>(0);
        public List<PlatformTD> Path2List = new List<PlatformTD>(0);
        public List<PlatformTD> Path3List = new List<PlatformTD>(0);

        //PlatformTD lastChild1;
        //PlatformTD lastChild2;
        //PlatformTD lastChild3;

        public static int buildNumber = 0;
        public bool buildATower = false;
        public float buildTimer = 0;

        public bool path1Full = false;
        public bool path2Full = false;
        public bool path3Full = false;
        public bool allPathsFull = false;

        public int path1RockThreat = 0;
        public int path1PaperThreat = 0;
        public int path1ScissorThreat = 0;
        public int path1DangerNR = 0;
        public string path1DangerType;

        public int path2RockThreat = 0;
        public int path2PaperThreat = 0;
        public int path2ScissorThreat = 0;
        public int path2DangerNR = 0;
        public string path2DangerType;

        public int path3RockThreat = 0;
        public int path3PaperThreat = 0;
        public int path3ScissorThreat = 0;
        public int path3DangerNR = 0;
        public string path3DangerType;

        public int pathsDangerNR = 0;

        // Use this for initialization
        void Start()
        {
            Path1 = GameObject.FindGameObjectWithTag("Path1").GetComponent<PathTD>();//GetComponent<PathTD>();
            Path2 = GameObject.FindGameObjectWithTag("Path2").GetComponent<PathTD>();
            Path3 = GameObject.FindGameObjectWithTag("Path3").GetComponent<PathTD>();
            PathPlatform1 = GameObject.FindGameObjectWithTag("PathPlatform1");
            PathPlatform2 = GameObject.FindGameObjectWithTag("PathPlatform2");
            PathPlatform3 = GameObject.FindGameObjectWithTag("PathPlatform3");
            //path = Path1.GetComponent<PathTD>();
            //Path1 = GameObject.FindGameObjectWithTag("Path1");
            PathStatic1 = Path1;//.GetComponent<PathTD>();
            PathStatic2 = Path2;
            PathStatic3 = Path3;

            foreach (Transform child in PathPlatform1.gameObject.transform)
            {
                //Debug.Log("Foreach loop: " + child);
                Path1List.Add(child.gameObject.GetComponent<PlatformTD>());
            }

            foreach (Transform child in PathPlatform2.gameObject.transform)
            {
                Path2List.Add(child.gameObject.GetComponent<PlatformTD>());
            }

            foreach (Transform child in PathPlatform3.gameObject.transform)
            {
                Path3List.Add(child.gameObject.GetComponent<PlatformTD>());
            }

            //lastChild1 = Path1List[Path1List.Count - 1];
            //lastChild2 = Path2List[Path2List.Count - 1];
            //lastChild3 = Path3List[Path3List.Count - 1];
        }

        // Update is called once per frame
        void Update()
        {
            if (buildNumber >= 1)
            {
                buildTimer += Time.deltaTime;
                if (buildTimer > 1f)
                {
                    buildATower = true;
                }
            }
            
            if (buildATower == true)
            {               
                    GetPathThreats();
                    DebugVariables();
                    PrioritisePath();
                    //CheckForSpots();
                    PathsFullOrFree();
                    GetABuildspot();
                    LowerPathThreatType();
                    BuildTower();
                    PathsFullOrFree();
                    buildTimer = 0; 
            }
            
        }

        public void GetPathThreats()
        {
            //path 1 threats
            path1RockThreat = SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().rockThreat;
            path1PaperThreat = SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().paperThreat;
            path1ScissorThreat = SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().scissorThreat;
            path1DangerNR = Mathf.Max(path1RockThreat, path1PaperThreat, path1ScissorThreat);
            path1DangerType = CheckPathDangerType(path1DangerType, path1DangerNR, path1RockThreat, path1PaperThreat, path1ScissorThreat);

            //path 2 threats
            path2RockThreat = SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().rockThreat;
            path2PaperThreat = SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().paperThreat;
            path2ScissorThreat = SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().scissorThreat;
            path2DangerNR = Mathf.Max(path2RockThreat, path2PaperThreat, path2ScissorThreat);
            path2DangerType = CheckPathDangerType(path2DangerType, path2DangerNR, path2RockThreat, path2PaperThreat, path2ScissorThreat);

            //path 3 threats
            path3RockThreat = SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().rockThreat;
            path3PaperThreat = SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().paperThreat;
            path3ScissorThreat = SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().scissorThreat;
            path3DangerNR = Mathf.Max(path3RockThreat, path3PaperThreat, path3ScissorThreat);
            path3DangerType = CheckPathDangerType(path3DangerType, path3DangerNR, path3RockThreat, path3PaperThreat, path3ScissorThreat);
        }

        public string CheckPathDangerType(string pathDangerType, int pathDangerNR, int rockThreat, int paperThreat, int scissorThreat)
        {
            if (rockThreat == pathDangerNR)// && pathDangerNR != 0)
            {
                pathDangerType = "rock";
            }
            if (paperThreat == pathDangerNR)// && pathDangerNR != 0)
            {
                pathDangerType = "paper";
            }
            if (scissorThreat == pathDangerNR)// && pathDangerNR != 0)
            {
                pathDangerType = "scissor";
            }

            if (rockThreat == pathDangerNR && paperThreat == pathDangerNR)// && pathDangerNR != 0)
            {
                int randomType = Random.Range(0, 2);
                switch(randomType)
                {
                    case 0:
                        pathDangerType = "rock";
                        break;
                    case 1:
                        pathDangerType = "paper";
                        break;
                }
            }

            if (rockThreat == pathDangerNR && scissorThreat == pathDangerNR)// && pathDangerNR != 0)
            {
                int randomType = Random.Range(0, 2);
                switch (randomType)
                {
                    case 0:
                        pathDangerType = "rock";
                        break;
                    case 1:
                        pathDangerType = "scissor";
                        break;
                }
            }

            if (paperThreat == pathDangerNR && scissorThreat == pathDangerNR)// && pathDangerNR != 0)
            {
                int randomType = Random.Range(0, 2);
                switch (randomType)
                {
                    case 0:
                        pathDangerType = "paper";
                        break;
                    case 1:
                        pathDangerType = "scissor";
                        break;
                }
            }
            return pathDangerType;
        }

        //Method for prioritising path and tower type
        public void PrioritisePath()
        {
            pathsDangerNR = Mathf.Max(path1DangerNR, path2DangerNR, path3DangerNR);

            if (path1DangerNR == pathsDangerNR)
            {
                priorityPath = Path1;
                priorityPathNR = 1;
                //priorityTowerType = path1DangerType;
            }

            if (path2DangerNR == pathsDangerNR)
            {
                priorityPath = Path2;
                priorityPathNR = 2;
                //priorityTowerType = path2DangerType;
            }

            if (path3DangerNR == pathsDangerNR)
            {
                priorityPath = Path3;
                priorityPathNR = 3;
                //priorityTowerType = path3DangerType;
            }

            if (path1DangerNR == pathsDangerNR && path2DangerNR == pathsDangerNR)
            {
                int randomPath = Random.Range(0, 2);
                switch (randomPath)
                {
                    case 0:
                        priorityPath = Path1;
                        priorityPathNR = 1;
                        //priorityTowerType = path1DangerType;
                        break;
                    case 1:
                        priorityPath = Path2;
                        priorityPathNR = 2;
                        //priorityTowerType = path2DangerType;
                        break;
                }
            }

            if (path1DangerNR == pathsDangerNR && path3DangerNR == pathsDangerNR)
            {
                int randomPath = Random.Range(0, 2);
                switch (randomPath)
                {
                    case 0:
                        priorityPath = Path1;
                        priorityPathNR = 1;
                        //priorityTowerType = path1DangerType;
                        break;
                    case 1:
                        priorityPath = Path3;
                        priorityPathNR = 3;
                        //priorityTowerType = path3DangerType;
                        break;
                }
            }

            if (path2DangerNR == pathsDangerNR && path3DangerNR == pathsDangerNR)
            {
                int randomPath = Random.Range(0, 2);
                switch (randomPath)
                {
                    case 0:
                        priorityPath = Path2;
                        priorityPathNR = 2;
                        //priorityTowerType = path2DangerType;
                        break;
                    case 1:
                        priorityPath = Path3;
                        priorityPathNR = 2;
                        //priorityTowerType = path3DangerType;
                        break;
                }
            }

            if (path1DangerNR == pathsDangerNR && path2DangerNR == pathsDangerNR && path3DangerNR == pathsDangerNR)
            {
                int randomPath = Random.Range(0, 3);
                switch (randomPath)
                {
                    case 0:
                        priorityPath = Path1;
                        priorityPathNR = 1;
                        //priorityTowerType = path1DangerType;
                        break;
                    case 1:
                        priorityPath = Path2;
                        priorityPathNR = 2;
                        //priorityTowerType = path2DangerType;
                        break;
                    case 2:
                        priorityPath = Path3;
                        priorityPathNR = 3;
                        //priorityTowerType = path3DangerType;
                        break;
                }
            }

        }

        //Build Tower Method
        public void BuildTower()
        {
            if (allPathsFull == false)
            {
                BuildManager.BuildMyTower(currentTower, CurrentPlatform.transform.position, Quaternion.identity, CurrentPlatform);    
            }
            buildATower = false;
            buildNumber -= 1;
        }

        public void BuildOnPath1()
        {
            foreach (PlatformTD child in Path1List)
            {
                if (child.GetComponent<PlatformStatus>().occupied == false)
                {
                    CurrentPlatform = child;
                    child.GetComponent<PlatformStatus>().occupied = true;
                    break;
                }
            }
        }

        public void BuildOnPath2()
        {
            foreach (PlatformTD child in Path2List)
            {
                if (child.GetComponent<PlatformStatus>().occupied == false)
                {
                    CurrentPlatform = child;
                    child.GetComponent<PlatformStatus>().occupied = true;
                    break;
                }
            }
        }

        public void BuildOnPath3()
        {
            foreach (PlatformTD child in Path3List)
            {
                if (child.GetComponent<PlatformStatus>().occupied == false)
                {
                    CurrentPlatform = child;
                    child.GetComponent<PlatformStatus>().occupied = true;
                    break;
                }
            }
        }

        public void GetABuildspot()
        {
            switch (priorityPathNR)
            {
                case 1:
                    if (path1Full == false)
                    {
                        BuildOnPath1();
                    }
                    else if (path2Full == false)
                    {
                        priorityPath = Path2;
                        priorityPathNR = 2;
                        BuildOnPath2();
                    }
                    else if (path3Full == false)
                    {
                        priorityPath = Path3;
                        priorityPathNR = 3;
                        BuildOnPath3();
                    }
                    else
                    {
                        allPathsFull = true;
                    }
                    break;
                case 2:
                    if (path2Full == false)
                    {
                        BuildOnPath2();
                    }
                    else if (path1Full == false)
                    {
                        priorityPath = Path1;
                        priorityPathNR = 1;
                        BuildOnPath1();
                    }
                    else if (path3Full == false)
                    {
                        priorityPath = Path3;
                        priorityPathNR = 3;
                        BuildOnPath3();
                    }
                    else
                    {
                        allPathsFull = true;
                    }
                    break;
                case 3:
                    if (path3Full == false)
                    {
                        BuildOnPath3();
                    }
                    else if (path1Full == false)
                    {
                        priorityPath = Path1;
                        priorityPathNR = 1;
                        BuildOnPath1();
                    }
                    else if (path2Full == false)
                    {
                        priorityPath = Path2;
                        priorityPathNR = 2;
                        BuildOnPath2();
                    }
                    else
                    {
                        allPathsFull = true;
                    }
                    break;
            }
            /*
            if (priorityPath == Path1)
            {
                if (path1Full == false)
                {
                    BuildOnPath1();
                }
                else
                {
                    if (path2Full == false)
                    {
                        BuildOnPath2();
                        priorityPath = Path2;
                        priorityPathNR = 2;
                    }
                    else
                    {
                        if (path3Full == false)
                        {
                            BuildOnPath3();
                            priorityPath = Path3;
                            priorityPathNR = 3;
                        }
                        else
                        {
                            allPathsFull = true;
                        }
                    }
                }
            }

            if (priorityPath == Path2)
            {
                if (path2Full == false)
                {
                    BuildOnPath2();
                }
                else
                {
                    if (path1Full == false)
                    {
                        BuildOnPath1();
                        priorityPath = Path1;
                        priorityPathNR = 1;
                    }
                    else
                    {
                        if (path3Full == false)
                        {
                            BuildOnPath3();
                            priorityPath = Path3;
                            priorityPathNR = 3;
                        }
                        else
                        {
                            allPathsFull = true;
                        }
                    }
                }
            }

            if (priorityPath == Path3)
            {
                if (path3Full == false)
                {
                    BuildOnPath3();
                }
                else
                {
                    if (path1Full == false)
                    {
                        BuildOnPath1();
                        priorityPath = Path1;
                        priorityPathNR = 1;
                    }
                    else
                    {
                        if (path2Full == false)
                        {
                            BuildOnPath2();
                            priorityPath = Path2;
                            priorityPathNR = 2;
                        }
                        else
                        {
                            allPathsFull = true;
                        }
                    }
                }
            }
            */
            GetTowerType();

        }

        public void PathsFullOrFree()
        {
            int spotsTaken1 = 0;
            foreach (PlatformTD child in Path1List)
            {
                if (child.GetComponent<PlatformStatus>().occupied == false)
                {
                    path1Full = false;
                    allPathsFull = false;
                }
                else
                {
                    spotsTaken1++;
                    if (spotsTaken1 == Path1List.Count)
                    {
                        path1Full = true;
                    }
                }
            }

            int spotsTaken2 = 0;
            foreach (PlatformTD child in Path2List)
            {
                if (child.GetComponent<PlatformStatus>().occupied == false)
                {
                    path2Full = false;
                    allPathsFull = false;
                }
                else
                {
                    spotsTaken2++;
                    if (spotsTaken2 == Path2List.Count)
                    {
                        path2Full = true;
                    }
                }
            }

            int spotsTaken3 = 0;
            foreach (PlatformTD child in Path3List)
            {
                if (child.GetComponent<PlatformStatus>().occupied == false)
                {
                    path3Full = false;
                    allPathsFull = false;
                }
                else
                {
                    spotsTaken3++;
                    if (spotsTaken3 == Path3List.Count)
                    {
                        path3Full = true;
                    }
                }
            }
        }
        /*
        //CheckPathsForSpots method
        public void CheckForSpots()
        {
            if (path1Full == true)
            {
                foreach (PlatformTD child in Path1List)
                {
                    if (child.GetComponent<PlatformStatus>().occupied == false)
                    {
                        path1Full = false;
                        allPathsFull = false;
                    }
                }
            }

            if (path2Full == true)
            {
                foreach (PlatformTD child in Path2List)
                {
                    if (child.GetComponent<PlatformStatus>().occupied == false)
                    {
                        path2Full = false;
                        allPathsFull = false;
                    }
                }
            }

            if (path3Full == true)
            {
                foreach (PlatformTD child in Path3List)
                {
                    if (child.GetComponent<PlatformStatus>().occupied == false)
                    {
                        path3Full = false;
                        allPathsFull = false;
                    }
                }
            }
        }*/

        public void GetTowerType()
        {
            if (priorityPath == Path1 && path1Full == false)
            {
                priorityTowerType = path1DangerType;
            }
            /*else if (priorityPath == Path1 && path1Full == true)
            {
                RandomTowerOnLeftoverSpots();
                return;
            }*/

            if (priorityPath == Path2 && path2Full == false)
            {
                priorityTowerType = path2DangerType;
            }
            /*else if (priorityPath == Path2 && path2Full == true)
            {
                RandomTowerOnLeftoverSpots();
                return;
            }*/

            if (priorityPath == Path3 && path3Full == false)
            {
                priorityTowerType = path3DangerType;
            }
            /*else if (priorityPath == Path3 && path3Full == true)
            {
                RandomTowerOnLeftoverSpots();
                return;
            }*/

            if (priorityTowerType == "rock")
            {
                currentTower = paperTower;
            }

            if (priorityTowerType == "paper")
            {
                currentTower = scissorTower;
            }

            if (priorityTowerType == "scissor")
            {
                currentTower = rockTower;
            }
        }

        public void LowerPathThreatType()
        {
            if (currentTower == rockTower)
            {
                switch (priorityPathNR)
                {
                    case 1:
                        SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().scissorThreat -= 1;
                        break;
                    case 2:
                        SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().scissorThreat -= 1;
                        break;
                    case 3:
                        SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().scissorThreat -= 1;
                        break;
                }
            }

            if (currentTower == paperTower)
            {
                switch (priorityPathNR)
                {
                    case 1:
                        SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().rockThreat -= 1;
                        break;
                    case 2:
                        SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().rockThreat -= 1;
                        break;
                    case 3:
                        SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().rockThreat -= 1;
                        break;
                }
            }

            if (currentTower == scissorTower)
            {
                switch (priorityPathNR)
                {
                    case 1:
                        SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().paperThreat -= 1;
                        break;
                    case 2:
                        SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().paperThreat -= 1;
                        break;
                    case 3:
                        SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().paperThreat -= 1;
                        break;
                }
            }
        }

        public void RandomTowerOnLeftoverSpots() //randomises tower type to place on spots that can still be built
        {
                int randomTower = Random.Range(0, 3);
                switch (randomTower)
                {
                    case 0:
                        currentTower = rockTower;
                        break;
                    case 1:
                        currentTower = paperTower;
                        break;
                    case 2:
                        currentTower = scissorTower;
                        break;
                }
        }

        public void DebugVariables()
        {
            Debug.Log("path2DangerNR " + path2DangerNR);
            Debug.Log("path2DangerType " + path2DangerType);
            Debug.Log("path2PaperThreat " + path2PaperThreat);
            Debug.Log("path2RockThreat " + path2RockThreat);
            Debug.Log("path2ScissorThreat " + path2ScissorThreat);
        }



    }

}