﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK
{

    public class AI : MonoBehaviour
    {

        public UnitTower currentTower;
        public UnitTower rockTower;
        public UnitTower paperTower;
        public UnitTower scissorTower;

        public PlatformTD CurrentPlatform;

        //Can be done more efficiently by using the children in paths in hierarchy
        public Transform CurrentPlatformTD;

        public PlatformTD PlatformTD1Path1;
        public PlatformTD PlatformTD2Path1;
        public PlatformTD PlatformTD3Path1;
        public PlatformTD PlatformTD4Path1;
        public PlatformTD PlatformTD5Path1;
        public PlatformTD PlatformTD6Path1;
        public PlatformTD PlatformTD7Path1;
        public PlatformTD PlatformTD8Path1;

        public PlatformTD PlatformTD1Path2;
        public PlatformTD PlatformTD2Path2;
        public PlatformTD PlatformTD3Path2;
        public PlatformTD PlatformTD4Path2;
        public PlatformTD PlatformTD5Path2;
        public PlatformTD PlatformTD6Path2;
        public PlatformTD PlatformTD7Path2;
        public PlatformTD PlatformTD8Path2;

        public PlatformTD PlatformTD1Path3;
        public PlatformTD PlatformTD2Path3;
        public PlatformTD PlatformTD3Path3;
        public PlatformTD PlatformTD4Path3;
        public PlatformTD PlatformTD5Path3;
        public PlatformTD PlatformTD6Path3;
        public PlatformTD PlatformTD7Path3;
        public PlatformTD PlatformTD8Path3;

        //List<string> theList = new List<string>(10);

        public List<PlatformTD> platformTDsPath1 = new List<PlatformTD>(0);
        public List<PlatformTD> platformTDsPath2 = new List<PlatformTD>(0);
        public List<PlatformTD> platformTDsPath3 = new List<PlatformTD>(0);

        public bool noTowerSelected = true;
        public bool buildNow = false;
        public bool path1Full = false;
        public bool path2Full = false;
        public bool path3Full = false;
        public static int canBuild = 1;
        public static bool buildDelayNow = false;

        public int towerNR = 1;
        public int priorityPath = 0;
        public string biggestThreatType = "";
        public int biggestThreat = 0;
        public int biggestRockThreat = 0;
        public int biggestPaperThreat = 0;
        public int biggestScissorThreat = 0;
        public int rockThreatPath = 0;
        public int paperThreatPath = 0;
        public int scissorThreatPath = 0;

        public Vector3 path1 = new Vector3(-4, 0, -7);
        public Vector3 path2 = new Vector3(-4, 0, -2.5f);
        public Vector3 path3 = new Vector3(-4, 0, 2.5f);
        public Vector3 path4 = new Vector3(-4, 0, 7);
        public float buildDelay = 0;   //Utilise timer together with bool buildNow
        //public int rockThreat;
        //public int paperThreat;
        //public int scissorThreat;

        // Use this for initialization
        void Start()
        {
            CurrentPlatformTD = PlatformTD1Path1.transform;
            CurrentPlatform = PlatformTD1Path1;
            platformTDsPath1.Add(PlatformTD1Path1);
            platformTDsPath1.Add(PlatformTD2Path1);
            platformTDsPath1.Add(PlatformTD3Path1);
            platformTDsPath1.Add(PlatformTD4Path1);
            platformTDsPath1.Add(PlatformTD5Path1);
            platformTDsPath1.Add(PlatformTD6Path1);
            platformTDsPath1.Add(PlatformTD7Path1);
            platformTDsPath1.Add(PlatformTD8Path1);

            platformTDsPath2.Add(PlatformTD1Path2);
            platformTDsPath2.Add(PlatformTD2Path2);
            platformTDsPath2.Add(PlatformTD3Path2);
            platformTDsPath2.Add(PlatformTD4Path2);
            platformTDsPath2.Add(PlatformTD5Path2);
            platformTDsPath2.Add(PlatformTD6Path2);
            platformTDsPath2.Add(PlatformTD7Path2);
            platformTDsPath2.Add(PlatformTD8Path2);

            platformTDsPath3.Add(PlatformTD1Path3);
            platformTDsPath3.Add(PlatformTD2Path3);
            platformTDsPath3.Add(PlatformTD3Path3);
            platformTDsPath3.Add(PlatformTD4Path3);
            platformTDsPath3.Add(PlatformTD5Path3);
            platformTDsPath3.Add(PlatformTD6Path3);
            platformTDsPath3.Add(PlatformTD7Path3);
            platformTDsPath3.Add(PlatformTD8Path3);
        }

        // Update is called once per frame
        void Update() {
            if (canBuild >= 2)
            {
                buildDelayNow = true;
                canBuild = 0;
            }
                if (buildDelayNow)
                {
                    buildDelay += Time.deltaTime;
                    if (buildDelay > 1)
                    {
                        buildNow = true;
                        buildDelay = 0;
                        buildDelayNow = false;
                    }

                }
            

            prioritisePath();

        if (buildNow)
        {
            CheckPaths();
            //prioritisePath();
            //Vector3 pos = new Vector3(1,1,1);
            //Quaternion rot = new Quaternion(1,1,1,1);
            //Vector3 pos = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            //Quaternion rot = SpawnManager.defPath.GetSpawnPoint().rotation;
            //GameObject obj = ObjectPoolManager.Spawn(currentTower, pos, rot);
            //GameObject obj = Instantiate(currentTower, pos, Quaternion.identity) as GameObject;
            //GameObject shadow = Instantiate(Shadow, spawnPosition, Quaternion.identity) as GameObject;
            //BuildManager.BuildMyTower(currentTower, pos, Quaternion.identity);
            //BuildManager.BuildMyTower(currentTower, pos1, Quaternion.identity);
            //BuildManager.BuildMyTower(currentTower, pos2, Quaternion.identity);
            //BuildManager.BuildMyTower(currentTower, pos3, Quaternion.identity);

            switch (priorityPath)
            {
                //case 1: //Should not be defPath but Path1, Path2 etc...
                    //BuildManager.BuildMyTower(currentTower, SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().pathPos, Quaternion.identity);
                    //SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().pathPos.x += 2;
                case 1:
                    
                    if (path1Full == false)
                    {
                        Debug.Log("Still Alive before TowerSelect");
                        TowerSelect();
                        Debug.Log("Still Alive after TowerSelect");
                        BuildManager.BuildMyTower(currentTower, CurrentPlatformTD.position, Quaternion.identity, CurrentPlatform);
                        buildNow = false;
                        LowerThreat();
                        return;
                    }
                    //path1.x += 2;
                    //buildNow = false;
                    //LowerThreat();
                    //CurrentPlatformTD = PlatformTD2;
                    else
                    {
                        priorityPath++;
                        break;
                    }
                case 2:
                    //Debug.Log("Still Alive before TowerSelect");
                    //TowerSelect();
                    //Debug.Log("Still Alive after TowerSelect");
                    if (path2Full == false)
                    {
                        Debug.Log("Still Alive before TowerSelect");
                        TowerSelect();
                        Debug.Log("Still Alive after TowerSelect");
                        BuildManager.BuildMyTower(currentTower, CurrentPlatformTD.position, Quaternion.identity, CurrentPlatform);
                        buildNow = false;
                        LowerThreat();
                        return;
                    }
                    //BuildManager.BuildMyTower(currentTower, path2, Quaternion.identity, CurrentPlatform);
                    //path2.x += 2;
                    //buildNow = false;
                    //LowerThreat();
                    else
                    {
                        priorityPath++;
                        break;
                    }
                case 3:
                    //Debug.Log("Still Alive before TowerSelect");
                    //TowerSelect();
                    //Debug.Log("Still Alive after TowerSelect");
                    if (path3Full == false)
                    {
                        Debug.Log("Still Alive before TowerSelect");
                        TowerSelect();
                        Debug.Log("Still Alive after TowerSelect");
                        BuildManager.BuildMyTower(currentTower, CurrentPlatformTD.position, Quaternion.identity, CurrentPlatform);
                        buildNow = false;
                        LowerThreat();
                        return;
                    }
                    else
                    {
                        //BuildManager.BuildMyTower(currentTower, path3, Quaternion.identity, CurrentPlatform);
                        //path3.x += 2;
                        priorityPath = 1;
                        break;
                    }
                /*case 4:
                    Debug.Log("Still Alive before TowerSelect");
                    TowerSelect();
                    Debug.Log("Still Alive after TowerSelect");
                    BuildManager.BuildMyTower(currentTower, CurrentPlatformTD.position, Quaternion.identity, CurrentPlatform);
                    //BuildManager.BuildMyTower(currentTower, path4, Quaternion.identity, CurrentPlatform);
                    //path4.x += 2;
                    buildNow = false;
                    LowerThreat();
                    return;*/
            }
            //BuildManager.BuildMyTower(currentTower, path1, Quaternion.identity);
            
            //UnitTower unit = obj.GetComponent<UnitTower>();
            //unit.Init();
            //buildNow = false;
            
        }
	}

        public void prioritisePath()
        { //Change defPath to a check of all Paths
            //Tell AI what enemy unit type is biggest threat
            biggestThreat = Mathf.Max(biggestRockThreat, biggestPaperThreat, biggestScissorThreat);

            biggestRockThreat = Mathf.Max(SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().rockThreat, SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().rockThreat, SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().rockThreat);

            biggestPaperThreat = Mathf.Max(SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().paperThreat, SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().paperThreat, SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().paperThreat);

            biggestScissorThreat = Mathf.Max(SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().scissorThreat, SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().scissorThreat, SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().scissorThreat);

            if (SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().rockThreat == biggestRockThreat)
            {
                if (path1Full == false)
                {
                    int rockPath = Random.Range(0, 1);
                    switch (rockPath)
                    {
                        case 0:
                            rockThreatPath = 1;
                            break;
                        case 1:
                            rockThreatPath = 2;
                            break;
                    }
                }
                
            }
            if (SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().rockThreat == biggestRockThreat)
            {
                if (path2Full == false)
                {
                    int rockPath = Random.Range(0, 1);
                    switch (rockPath)
                    {
                        case 0:
                            rockThreatPath = 2;
                            break;
                        case 1:
                            rockThreatPath = 3;
                            break;
                    }
                }
                
            }
            if (SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().rockThreat == biggestRockThreat)
            {
                if (path3Full == false)
                {
                    int rockPath = Random.Range(0, 1);
                    switch (rockPath)
                    {
                        case 0:
                            rockThreatPath = 3;
                            break;
                        case 1:
                            rockThreatPath = 4;
                            break;
                    }
                }
            }

            if (SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().paperThreat == biggestPaperThreat)
            {
                if (path1Full == false)
                {
                    int paperPath = Random.Range(0, 1);
                    switch (paperPath)
                    {
                        case 0:
                            paperThreatPath = 1;
                            break;
                        case 1:
                            paperThreatPath = 2;
                            break;
                    }
                }

            }
            if (SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().paperThreat == biggestPaperThreat)
            {
                if (path2Full == false)
                {
                    int paperPath = Random.Range(0, 1);
                    switch (paperPath)
                    {
                        case 0:
                            paperThreatPath = 2;
                            break;
                        case 1:
                            paperThreatPath = 3;
                            break;
                    }
                }
            }
            if (SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().paperThreat == biggestPaperThreat)
            {
                if (path3Full == false)
                {
                    int paperPath = Random.Range(0, 1);
                    switch (paperPath)
                    {
                        case 0:
                            paperThreatPath = 3;
                            break;
                        case 1:
                            paperThreatPath = 4;
                            break;
                    }
                }
            }

            if (SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().scissorThreat == biggestScissorThreat)
            {
                if (path1Full == false)
                {
                    int scissorPath = Random.Range(0, 1);
                    switch (scissorPath)
                    {
                        case 0:
                            scissorThreatPath = 1;
                            break;
                        case 1:
                            scissorThreatPath = 2;
                            break;
                    }
                }
            }
            if (SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().scissorThreat == biggestScissorThreat)
            {
                if (path2Full == false)
                {
                    int scissorPath = Random.Range(0, 1);
                    switch (scissorPath)
                    {
                        case 0:
                            scissorThreatPath = 2;
                            break;
                        case 1:
                            scissorThreatPath = 3;
                            break;
                    }
                }
            }
            if (SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().scissorThreat == biggestScissorThreat)
            {
                if (path3Full == false)
                {
                    int scissorPath = Random.Range(0, 1);
                    switch (scissorPath)
                    {
                        case 0:
                            scissorThreatPath = 3;
                            break;
                        case 1:
                            scissorThreatPath = 4;
                            break;
                    }
                }
            }

            if (biggestRockThreat == biggestThreat)
            {
                biggestThreatType = "rock";
                priorityPath = rockThreatPath;
                currentTower = paperTower;
            }
            if (biggestPaperThreat == biggestThreat)
            {
                biggestThreatType = "paper";
                priorityPath = paperThreatPath;
                currentTower = scissorTower;
            }
            if (biggestScissorThreat == biggestThreat)
            {
                biggestThreatType = "scissor";
                priorityPath = scissorThreatPath;
                currentTower = rockTower;
            }

        }

        public void LowerThreat()
        {
            switch (priorityPath)
            {
                case 1:
                    if(biggestThreatType == "rock")
                    {
                        SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().rockThreat-=2;
                    }
                    if (biggestThreatType == "paper")
                    {
                        SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().paperThreat-=2;
                    }
                    if (biggestThreatType == "scissor")
                    {
                        SpawnManager.pathstatic1.GetComponentInChildren<MouseClicked>().scissorThreat-=2;
                    }
                    break;
                case 2: //Does NOT reflect correct spawn path, AI paths and Unit spawn paths are not the same!
                    if (biggestThreatType == "rock")
                    {
                        SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().rockThreat -= 2;
                    }
                    if (biggestThreatType == "paper")
                    {
                        SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().paperThreat -= 2;
                    }
                    if (biggestThreatType == "scissor")
                    {
                        SpawnManager.pathstatic2.GetComponentInChildren<MouseClicked>().scissorThreat -= 2;
                    }
                    break;
                case 3:
                    if (biggestThreatType == "rock")
                    {
                        SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().rockThreat -= 2;
                    }
                    if (biggestThreatType == "paper")
                    {
                        SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().paperThreat -= 2;
                    }
                    if (biggestThreatType == "scissor")
                    {
                        SpawnManager.pathstatic3.GetComponentInChildren<MouseClicked>().scissorThreat -= 2;
                    }
                    break;
            }
        }

        public void TowerSelect()
        {
            
            Debug.Log("Still Alive start TowerSelect");
            while (noTowerSelected == true)
            {
                Debug.Log("Still Alive start while");

                //for way
                switch (priorityPath)
                {
                    case 1:
                        for (int i = 0; i <= platformTDsPath1.Count; i++)
                        {
                            Debug.Log("start for " + i);
                            if (i >= platformTDsPath1.Count)
                            {
                                noTowerSelected = false;
                                path1Full = true; 
                                break;
                            }
                            Debug.Log("Still Alive i =" + i);
                            PlatformTD platformTD = platformTDsPath1[i];

                            if (platformTD.GetComponent<PlatformStatus>().occupied == false)
                            {
                                Debug.Log("Still Alive for --> IF" + i);
                                CurrentPlatformTD = platformTD.transform;
                                CurrentPlatform = platformTD;
                                platformTD.GetComponent<PlatformStatus>().occupied = true;
                                noTowerSelected = false;
                                path1Full = false; 
                                break;
                            }
                            towerNR++;
                            Debug.Log("Still Alive for end");
                        }
                        break;
                    case 2:
                        for (int i = 0; i <= platformTDsPath2.Count; i++)
                        {
                            Debug.Log("start for " + i);
                            if (i >= platformTDsPath2.Count)
                            {
                                noTowerSelected = false;
                                path2Full = true;
                                break;
                            }
                            Debug.Log("Still Alive i =" + i);
                            PlatformTD platformTD = platformTDsPath2[i];

                            if (platformTD.GetComponent<PlatformStatus>().occupied == false)
                            {
                                Debug.Log("Still Alive for --> IF" + i);
                                CurrentPlatformTD = platformTD.transform;
                                CurrentPlatform = platformTD;
                                platformTD.GetComponent<PlatformStatus>().occupied = true;
                                noTowerSelected = false;
                                path2Full = false; 
                                break;
                            }
                            towerNR++;
                            Debug.Log("Still Alive for end");
                        }
                        break;
                    case 3:
                        for (int i = 0; i <= platformTDsPath3.Count; i++)
                        {
                            Debug.Log("start for " + i);
                            if (i >= platformTDsPath3.Count)
                            {
                                noTowerSelected = false;
                                path3Full = true; 
                                break;
                            }
                            Debug.Log("Still Alive i =" + i);
                            PlatformTD platformTD = platformTDsPath3[i];

                            if (platformTD.GetComponent<PlatformStatus>().occupied == false)
                            {
                                Debug.Log("Still Alive for --> IF" + i);
                                CurrentPlatformTD = platformTD.transform;
                                CurrentPlatform = platformTD;
                                platformTD.GetComponent<PlatformStatus>().occupied = true;
                                noTowerSelected = false;
                                path3Full = false; 
                                break;
                            }
                            towerNR++;
                            Debug.Log("Still Alive for end");
                        }
                        break;
                }
                
                /*
                //foreach way
                    foreach (PlatformTD platformTD in platformTDArray)
                {
                    PlatformTD thisPlatform = platformTD.GetComponent<PlatformTD>();
                    Debug.Log("Still Alive " + thisPlatform);
                    if (thisPlatform.GetComponent<PlatformStatus>().occupied == false)
                    {
                        Debug.Log("Still Alive foreach --> IF");
                        CurrentPlatformTD = thisPlatform.transform;
                        CurrentPlatform = thisPlatform;
                        thisPlatform.GetComponent<PlatformStatus>().occupied = true;
                        noTowerSelected = false;
                        //break;
                    }
                    towerNR++;
                    Debug.Log("Still Alive for end");   

                }*/

                /* //Switch way
                switch (towerNR)
                {     
                    //case 0:
                    //    towerNR++;
                    //    break;
                    case 1:
                        Debug.Log("Still Alive case 1");
                        if(PlatformTD1.GetComponent<PlatformStatus>().occupied == false)
                        {
                            Debug.Log("Still Alive IF");
                            CurrentPlatformTD = PlatformTD1.transform;
                            CurrentPlatform = PlatformTD1;
                            PlatformTD1.GetComponent<PlatformStatus>().occupied = true;
                            noTowerSelected = false;
                            Debug.Log("Still Alive noTowerSelected false");
                        } 
                        towerNR++;
                        Debug.Log("Still Alive case 1 end");
                        break;
                    case 2:
                        Debug.Log("Still Alive case 2");
                        if (PlatformTD2.GetComponent<PlatformStatus>().occupied == false)
                        {
                            Debug.Log("Still Alive IF2");
                            CurrentPlatformTD = PlatformTD2.transform;
                            CurrentPlatform = PlatformTD2;
                            PlatformTD2.GetComponent<PlatformStatus>().occupied = true;
                            noTowerSelected = false;
                        }
                        towerNR++;
                        Debug.Log("Still Alive case 2 end");
                        break;
                    case 3:
                        //if (PlatformTD3.GetComponent<PlatformStatus>().occupied == false)
                        //{
                            CurrentPlatformTD = PlatformTD3.transform;
                            CurrentPlatform = PlatformTD3;
                            PlatformTD3.GetComponent<PlatformStatus>().occupied = true;
                            //towerNR++;
                            noTowerSelected = false;
                        //}
                        break;
                }*/
                Debug.Log("Still Alive end while");
            }
            towerNR = 1;
            noTowerSelected = true;
            Debug.Log("Still Alive end TowerSelect");
        }

        public void CheckPaths()
        {
                    for (int i = 0; i <= platformTDsPath1.Count; i++)
                    {
                        if (i >= platformTDsPath1.Count)
                        {
                            break;
                        }
                        Debug.Log("Checking Path1 spot " + i);
                        PlatformTD platformTD = platformTDsPath1[i];

                        if (platformTD.GetComponent<PlatformStatus>().occupied == false)
                        {
                            Debug.Log("Empty spot " + i);
                            path1Full = false;
                            break;
                        }
                        Debug.Log("Still Alive end path1Check");
                    }

                    for (int i = 0; i <= platformTDsPath2.Count; i++)
                    {
                        if (i >= platformTDsPath2.Count)
                        {
                            break;
                        }
                        Debug.Log("Checking Path2 spot " + i);
                        PlatformTD platformTD = platformTDsPath2[i];

                        if (platformTD.GetComponent<PlatformStatus>().occupied == false)
                        {
                            Debug.Log("Empty spot " + i);
                            path2Full = false;
                            break;
                        }
                        Debug.Log("Still Alive end path2Check");
                    }

                    for (int i = 0; i <= platformTDsPath3.Count; i++)
                    {
                        if (i >= platformTDsPath3.Count)
                        {
                            break;
                        }
                        Debug.Log("Checking Path3 spot " + i);
                        PlatformTD platformTD = platformTDsPath3[i];

                        if (platformTD.GetComponent<PlatformStatus>().occupied == false)
                        {
                            Debug.Log("Empty spot " + i);
                            path3Full = false;
                            break;
                        }  
                        Debug.Log("Still Alive end path3Check");
            }
        }

    }

}