﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK
{

    public class TutorialSprites : MonoBehaviour
    {
        public string TutorialLevel;
        public Text txtTitle;
        public Text txtDesp;
        public GameObject butContinueObj;
        public GameObject butBackObj;
        public int gameMessage;
        public TutorialSpriteSwitch screen;

        // Use this for initialization
        void Start()
        {
            gameMessage = 1;
            txtTitle.text = "Screen 1 of 10";
            txtDesp.text = "This is your unit icon. 1 is your unit, 2 is where your unit is strong against, 3 is where your unit is weak against.";
            screen = FindObjectOfType<TutorialSpriteSwitch>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnContinueButton()
        {
            gameMessage++;
            screen.sprite++;
            screen.ChangeScreen();
            ShowScreen();
        }

        public void OnBackButton()
        {
            if (gameMessage > 1)
            {
                gameMessage--;
                screen.sprite--;
                screen.ChangeScreen();
                ShowScreen();
            }
        }

        public void ShowScreen()
        {
            switch (gameMessage)
            {
                case 1:
                    txtTitle.text = "Screen 1 of 10";
                    txtDesp.text = "This is your unit icon. 1 is your unit, 2 is where your unit is strong against, 3 is where your unit is weak against.";
                    break;
                case 2:
                    txtTitle.text = "Screen 2 of 10";
                    txtDesp.text = "Tap/touch a pulsing square at the start of a path to spawn your unit(1) on that path.";
                    //txtDesp.text = "Wake up Grab a brush and put a little (makeup) Grab a brush and put a little Hide the scars to fade away the (shakeup) Hide the scars to fade away the Whyd you leave the keys upon the table? Here you go create another fable You wanted to Grab a brush and put a little makeup You wanted to Hide the scars to fade away the shakeup You wanted to Whyd you leave the keys upon the table? You wanted to I dont think you trust In, my, self righteous suicide I, cry, when angels deserve to die, DIE"; //Wake up Grab a brush and put a little (makeup) Grab a brush and put a little Hide the scars to fade away the (shakeup) Hide the scars to fade away the Whyd you leave the keys upon the table? Here you go create another fable You wanted to Grab a brush and put a little makeup You wanted to Hide the scars to fade away the shakeup You wanted to Whyd you leave the keys upon the table? You wanted to I dont think you trust In, my, self righteous suicide I, cry, when angels deserve to die In, my, self righteous suicide I, cry, when angels deserve to die Father, father, father, father Father into your hands, I commend my spirit Father into your hands why have you forsaken me In your eyes forsaken me In your thoughts forsaken me In your heart forsaken, me oh Trust in my self righteous suicide I, cry, when angels deserve to die In my self righteous suicide I, cry, when angels deserve to die ";
                    break;
                case 3:
                    txtTitle.text = "Screen 3 of 10";
                    txtDesp.text = "Your unit will walk along the path until it reaches the exit(at the checkmark) or dies on the way there.";
                    break;
                case 4:
                    txtTitle.text = "Screen 4 of 10";
                    txtDesp.text = "This(2) is the enemy unit where your unit is strong against, this means your unit will defeat this unit.";
                    break;
                case 5:
                    txtTitle.text = "Screen 5 of 10";
                    txtDesp.text = "The enemy unit is destroyed, your unit has taken damage from this enemy. Even though your unit is strong against this enemy, it will die if it gets hit by 3 of these units.";
                    break;
                case 6:
                    txtTitle.text = "Screen 6 of 10";
                    txtDesp.text = "On this path you see another enemy unit(3), as you can see in your unit icon this is the enemy your unit is weak against.";
                    break;
                case 7:
                    txtTitle.text = "Screen 7 of 10";
                    txtDesp.text = "The result is that your unit dies and the enemy unit has taken damage.";
                    break;
                case 8:
                    txtTitle.text = "Screen 8 of 10";
                    txtDesp.text = "The last enemy unit(4) here is not indicated in your unit's strength or weakness. This means your unit and the enemy are equal.";
                    break;
                case 9:
                    txtTitle.text = "Screen 9 of 10";
                    txtDesp.text = "The result is that your unit and the enemy will both die.";
                    break;
                case 10:
                    txtTitle.text = "Screen 10 of 10";
                    txtDesp.text = "This is the Info Display. Top Left: number of units that need to cross the border. Bottom Left: You see how many of your units have spawned. Timer shows the time left before you can spawn the unit. Top Right: Pause Menu button.";
                    break;     
                case 11:
                    txtTitle.text = "Tutorial Completed";
                    txtDesp.text = "You now know all the basics, good luck. Tap/touch Continue to start playing.";
                    break;
                case 12:
                    //screen.sprite = 1;
                    LoadingScreen.Load(TutorialLevel);
                    //Application.LoadLevel(TutorialLevel);
                    break;
            }
        }

    }
}