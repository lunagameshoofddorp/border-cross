﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{
    public class ColorLerp : MonoBehaviour
    {
        //public static Color Lerp(Color a, Color b, float t);
        //public Color lerpedColor = Color.blue;
        void Update()
        {
            //lerpedColor = Color.Lerp(Color.blue, Color.green, Time.time);
            DaMethod();
        }

        void DaMethod()
        {
            this.gameObject.renderer.material.color = Color.Lerp(Color.blue, Color.green, Time.deltaTime);
        }
    }
}