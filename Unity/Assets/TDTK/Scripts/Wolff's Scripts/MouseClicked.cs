﻿using UnityEngine;
using System.Collections;

namespace TDTK
{

    public class MouseClicked : MonoBehaviour
    {

        public static bool mouseClicked = false;
        public int thisPath;
        public int rockThreat;
        public int paperThreat;
        public int scissorThreat;
        public GameControl selectedPath;

        public Vector3 pathPos = new Vector3(-4, 0, -7);

        // Use this for initialization
        void Start() {
            selectedPath = GameObject.FindObjectOfType<GameControl>();
	    }

        // Update is called once per frame
        void Update()
        {
            //Debug.Log(gameObject.name);
            //var selectedPath = GameObject.FindObjectOfType<GameControl>();

            if (selectedPath.pathSelected != thisPath)
            {
                this.gameObject.renderer.material.color = Color.white;
                mouseClicked = false;
            }
        }

        void OnMouseDown()
    {
        var selectedPath = GameObject.FindObjectOfType<GameControl>();

        if (Input.GetMouseButtonDown(0) && mouseClicked == false)
        {
            Debug.Log("OnMouseDown Done!");
            SpawnManager.clicked = true;
            mouseClicked = true;
            //this.gameObject.renderer.material.SetColor(Blue, Color.blue);
            this.gameObject.renderer.material.color = Color.yellow;
            selectedPath.pathSelected = thisPath;
            Debug.Log(thisPath + " is now the selected path!");
            //pathPos.x += 2; //Relocate to relevant position
           // rockThreat++; //Change to reflect unit type
            /*
            if (SpawnManager.currentUnitName == "rock" && SpawnManager.clicked == true)
            {
                rockThreat++;
            }
            if (SpawnManager.currentUnitName == "paper" && SpawnManager.clicked == true)
            {
                paperThreat++;
            }
            if (SpawnManager.currentUnitName == "scissor" && SpawnManager.clicked == true)
            {
                scissorThreat++;
            }
            */
            
        }

        mouseClicked = true;
    }
    }

}