﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class SandStorm : MonoBehaviour
    {

        public SandStorm sandStorm;

        public bool operationDesertStorm = false;
        public float timeInterval;

        public Transform[] allSand;

        public float sandStormTimer = 0;

        // Use this for initialization
        void Start()
        {
            allSand = sandStorm.GetComponentsInChildren<Transform>();
        }

        // Update is called once per frame
        void Update()
        {
            sandStormTimer += Time.deltaTime;

            if (sandStormTimer > timeInterval)
            {
                Debug.Log("Gogogo");
                sandStormTimer = 0;
                ActivateSandStorm(); 
            }
            /*
            if (operationDesertStorm == true)
            {
                ActivateSandStorm();
                operationDesertStorm = false;
            }*/
        }

        public void ActivateSandStorm()
        {
            //sandStorm.Play();
            //sandStorm.GetComponent<ParticleSystem>().Play();
            foreach (Transform sand in allSand)
            {
                Debug.Log("Darude...");
                    var storm = sand.GetComponent<ParticleSystem>();
                    storm.Play();
            }
        }

    }

}