﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;


namespace TDTK
{
    public class RandomMainMenuAnimation : MonoBehaviour
    {

        public enum _AniType { None, Mecanim, Legacy }
        public _AniType type = _AniType.None;

        public GameObject aniRootObj;

        public AnimationClip behaviour1;
        public AnimationClip behaviour2;
        public AnimationClip behaviour3;
        public AnimationClip behaviour4;

        public Animator anim;

        public bool goAnim = true;

        void Update()
        {
            if (type == _AniType.Mecanim && anim != null && goAnim == true)
            {
                PlayAnimation();
            }
        }

        void OnEnable()
        {
            if (type == _AniType.None) return;
        }

        public float PlayRandomAnimation()
        {
            if (type == _AniType.Mecanim) return GetRandomAnimation();
            return 0;
        }

        public float GetRandomAnimation()
        {
            anim.SetTrigger("AnimationGo");
            int randomAnimation = Random.Range(1, 5);
            anim.SetInteger("RandomAnimation", randomAnimation);
            //anim.SetTrigger("AnimationGo");

            if (randomAnimation == 1 || randomAnimation == 2 || randomAnimation == 3 || randomAnimation == 4)
            {
                switch (randomAnimation)
                {
                    case 1:
                        return behaviour1 != null ? behaviour1.length : 0;
                    case 2:
                        return behaviour2 != null ? behaviour2.length : 0;
                    case 3:
                        return behaviour3 != null ? behaviour3.length : 0;
                    case 4:
                        return behaviour4 != null ? behaviour4.length : 0;
                }
            }
            else
            {
                return behaviour1 != null ? behaviour1.length : 0;
            }

            return behaviour1 != null ? behaviour1.length : 0;
        }


        public void PlayAnimation()
        {
            goAnim = false;
            float delay=0;
            delay = PlayRandomAnimation();
            //anim.SetTrigger("AnimationGo");
            StartCoroutine(_ReachDestination(delay));
        }
        IEnumerator _ReachDestination(float duration)
        {
            //Debug.Log("before");
            yield return new WaitForSeconds(duration);
            //Debug.Log("after");
            goAnim = true;
        }

    }
}