﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK
{

    public class LoadingScreenDynamic : MonoBehaviour
    {

        public GameObject loadingScreenPanel;
        static LoadingScreenDynamic instance;
        public GameObject uiStuffToTurnOff1;
        public GameObject uiStuffToTurnOff2;
        public GameObject uiStuffToTurnOff3;
        public GameObject uiStuffToTurnOff4;

        void Awake()
        {
            //Debug.Log("awake");
            if (instance)
            {
                //Debug.Log("instance true");
                //Destroy(gameObject);
                instance.loadingScreenPanel.SetActive(false);
                return;
            }
            //Debug.Log("instance false");
            instance = this;
        }

        // Use this for initialization
        void Start()
        {
            instance.loadingScreenPanel.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public static void Load(int index)
        {
            /*
            if (FindObjectOfType<UI>() != null)
            {
                var allUI = FindObjectOfType<UI>();
                allUI.gameObject.SetActive(false);
            }*/
            TurnOtherUIOff();
            instance.loadingScreenPanel.SetActive(true);
            Application.LoadLevel(index);
        }

        public static void Load(string name)
        {
            /*
            if (FindObjectOfType<UI>() != null)
            {
                var allUI = FindObjectOfType<UI>();
                allUI.gameObject.SetActive(false);
            }*/
            TurnOtherUIOff();
            try
            {
                instance.loadingScreenPanel.SetActive(true);
            }
            catch
            {
                Debug.Log("LoadingScreen already done");
            }
            Application.LoadLevel(name);

        }

        public static void LoadWithExtraTime(string name)
        {
            Debug.Log("LoadWithExtraTime");
            instance.LoadExtraTime(name);
        }


        public void LoadExtraTime(string name)
        {
            /*
            if (FindObjectOfType<UI>() != null)
            {
                var allUI = FindObjectOfType<UI>();
                allUI.gameObject.SetActive(false);
            }*/
            TurnOtherUIOff();
            try
            {
                instance.loadingScreenPanel.SetActive(true);
            }
            catch
            {
                Debug.Log("LoadingScreen already done");
            }
            StartCoroutine(_ExtraTime(5f, name));
            Debug.Log("NowLoading");
            //Application.LoadLevel(name);

        }

        IEnumerator _ExtraTime(float duration, string name)
        {
            Debug.Log("_ExtraTime");
            yield return new WaitForSeconds(duration);
            Application.LoadLevel(name);
        }

        public static void TurnOtherUIOff()
        {
            try
            {
                if (instance.uiStuffToTurnOff1 != null)
                {
                    instance.uiStuffToTurnOff1.SetActive(false);
                }
            }

            catch
            {
                Debug.Log("No uiStuffToTurnOff1 GameObject");
            }

            try
            {
                if (instance.uiStuffToTurnOff2 != null)
                {
                    instance.uiStuffToTurnOff2.SetActive(false);
                }
            }

            catch
            {
                Debug.Log("No uiStuffToTurnOff2 GameObject");
            }

            try
            {
                if (instance.uiStuffToTurnOff3 != null)
                {
                    instance.uiStuffToTurnOff3.SetActive(false);
                }
            }

            catch
            {
                Debug.Log("No uiStuffToTurnOff3 GameObject");
            }
        }

        static bool NoInstance()
        {
            if (!instance)
                Debug.LogError("Loading Screen is not existing in scene.");
            return instance;
        }

    }

}