﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class ArrowHover : MonoBehaviour
    {

        public float Step; //A variable we increment
        public float Offset; //How far to offset the object upwards

        void Start()
        {
            //Store where we were placed in the editor
            var InitialPosition = transform.position;
            //Create an offset based on our height
            Offset = this.transform.localPosition.x;
        }

        void Update()
        {

            if (this.transform.localPosition.x < (Offset + 0.5))
            {
                Step -= 0.001f;
            }

          //Make sure Steps value never gets too out of hand 
            else
            {
                Step += 0.001f;
                //Debug.Log("jjjjj");
            }

            //Float up and down along the y axis, 
            transform.Translate(Step, 0, 0);

        }
    }

}