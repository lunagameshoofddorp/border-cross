﻿using UnityEngine;
using System.Collections;
using TDTK;

namespace TDTK
{

    public class ParticleSynch : MonoBehaviour
    {

        void Awake()
        {
            var allParticles = FindObjectsOfType<ParticleSystem>();
            foreach(ParticleSystem particle in allParticles)
            {
                particle.Play();
            }
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}