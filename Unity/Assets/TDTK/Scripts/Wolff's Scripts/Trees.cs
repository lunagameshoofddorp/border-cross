﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class Trees : MonoBehaviour
    {
        public Mesh HQMTree;
        public Mesh HQLTree;
        public Trees forest;
        public static bool changeTrees = false;
        //public GameObject tree;

        // Use this for initialization
        void Start()
        {
            //forest = this;
            int systemMemory = SystemInfo.systemMemorySize;
            if (systemMemory > 1000)
            {
                changeTrees = true;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (changeTrees == true)
            {
                Debug.Log("Change Tree meshes");
                ChangeMeshes();
                changeTrees = false;
            }
        }
        /*
        public void ChangeMeshes()
        {
            foreach(Transform tree in forest.transform)
            {
                //var treeMesh = tree.GetComponent<MeshFilter>();
                if (tree.name == "tree_medium_mesh")
                {
                    var treeMesh = tree.GetComponent<MeshFilter>();
                    treeMesh.mesh = lowQMTree;
                }
                else if (tree.name == "tree_large_mesh")
                {
                    var treeMesh = tree.GetComponent<MeshFilter>();
                    treeMesh.mesh = lowQLTree;
                }
            }
        }*/

        public void ChangeMeshes()
        {
            Transform[] allTrees = forest.GetComponentsInChildren<Transform>();
            foreach(Transform tree in allTrees)
            {
                //var treeMesh = tree.GetComponent<MeshFilter>();
                if (tree.name == "tree_medium_mesh")
                {
                    var treeMesh = tree.GetComponent<MeshFilter>();
                    treeMesh.mesh = HQMTree;
                }
                else if (tree.name == "tree_large_mesh")
                {
                    var treeMesh = tree.GetComponent<MeshFilter>();
                    treeMesh.mesh = HQLTree;
                }
            }
        }

    }

}