﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK
{

    public class UI_Pop_Up : MonoBehaviour {


        private GameObject thisObj;
		private static UI_Pop_Up instance;
		
		public Text txtTitle;
        public Text txtDesp;
		public GameObject butContinueObj;
        public int gameMessage;
		
		void Awake(){
			instance=this;
            
			thisObj=gameObject;
			
			transform.localPosition=Vector3.zero;
            Hide();
		}
		
		// Use this for initialization
		void Start () {
            //Time.timeScale = 0;
            PickInfoMessage();
			//Hide();
		}
		
		
		
		public void OnContinueButton(){
            //Debug.Log("Next Scene name " + GameControl.nextSceneStatic);
			Time.timeScale=1;
            Hide(); 
		}
		
		
		public static bool isOn=true;

		public static void Show(){ instance._Show(); }

		public void _Show(){	
			isOn=true;
			thisObj.SetActive(isOn);
            //Time.timeScale = 0;
		}

		public static void Hide(){ instance._Hide(); }

		public void _Hide(){
			isOn=false;
			thisObj.SetActive(isOn);
		}

        public void PickInfoMessage()
        {
            switch (GameControl.GetLevelID())
            {
                case 9:
                    txtDesp.text = "You Crossed All Borders! New Borders Coming Soon!";
                        break;
                    /*
                case 0:
                    txtDesp.text = "Welcome to the game. You need to get 1 unit to the end of the path (across the border) to win. Use the correct unit on the correct path. Look closely at your current unit it's strength and weakness to decide which path you click.";
                    break;
                case 1:
                    txtDesp.text = "Welcome to the first level, Press a pulsing area to spawn the unit indicated on the bottom left on that path. Note: 'Most levels have different playouts, this means the units you get may vary in order each time you play the level again.' Good luck Noob.";
                    break;
                case 2:
                    txtDesp.text = "Welcome to the second level, different border patrol setup now so try your best. Note: 'Some levels require that you use all the given paths in order to win.' Good luck Scrub.";
                    break;
                case 3:
                    txtDesp.text = "Welcome to the third level. Note: 'Your opponents, also known as the border patrol units, are the bad guys. So have no shame in defeating as much of them as you can while winning.' Good luck Greeny.";
                    break;
                case 4:
                    txtDesp.text = "Welcome to the fourth level, you get 1 unit less, deal with it. Hint: 'Time is of the essence'. Note: 'Some levels have only one or a few playouts.' Good luck Newb.";
                    break;
                case 5:
                    txtDesp.text = "Welcome to the fifth level, you get 4 units now, use them wisely. Note: 'If your unit is going down, it may still be able to fire a last shot.' Good luck Donut.";
                    break;*/
                /*case 5:
                    txtDesp.text = "Hint: 'Notes contain usefull information' Note: 'Some Hints teach you great wisdom.' Good luck Newb.";
                    break;*/
            }
        }
        /*
        public void MessageInGameplay()
        {
            switch (gameMessage)
            {
                case 0:
                    txtDesp.text = "Even though your units are good against the enemy that is showed in green, if your unit gets hit by 3 units which it is strong against it will die. Sometimes it is necessary to sacrifice a unit in order to clear a path for another unit.";
                    break;
                case 1:
                    txtDesp.text = "";
                    break;
                case 2:
                    txtDesp.text = "";
                    break;
                case 3:
                    txtDesp.text = "";
                    break;
                case 4:
                    txtDesp.text = "";
                    break;
            }
        }*/
		
	}


}