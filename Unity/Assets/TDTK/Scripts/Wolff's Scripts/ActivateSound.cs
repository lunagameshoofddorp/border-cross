﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class ActivateSound : MonoBehaviour
    {

        public bool explosion;
        public bool death;
        public bool gun;
        public bool explosiveShot;
        public WolffAudioManager audioPlayer;

        // Use this for initialization
        void Start()
        {
            audioPlayer = FindObjectOfType<WolffAudioManager>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void PlayExplosionSound()
        {
            //WolffAudioManager.PlayStaticSoundEffect(WolffAudioManager.explosionStaticSoundEffect);
            if (audioPlayer != null)
            {
                audioPlayer.PlaySoundEffect(audioPlayer.explosionSoundEffect);
            }
        }

        public void PlayGunSound()
        {
            //WolffAudioManager.PlayStaticSoundEffect(WolffAudioManager.explosionStaticSoundEffect);
            if (audioPlayer != null)
            {
                audioPlayer.PlaySoundEffect(audioPlayer.shootGunSoundEffect);
            }
        }

        public void PlayExplosiveShotSound()
        {
            //WolffAudioManager.PlayStaticSoundEffect(WolffAudioManager.explosionStaticSoundEffect);
            if (audioPlayer != null)
            {
                audioPlayer.PlaySoundEffect(audioPlayer.shootExplosionSoundEffect);
            }
        }

        public void PlayDeathSound()
        {
            if (audioPlayer != null)
            {
                audioPlayer.PlayDeathSoundEffect();
            }
        }

        public void OnDestroy()
        {
            if (audioPlayer != null)
            {
                //Debug.Log("on destroy");
                if (explosion == true)
                {
                    //Debug.Log("if statement");
                    PlayExplosionSound();
                }
                else if (death == true)
                {
                    PlayDeathSound();
                }
            }
        }

    }

}