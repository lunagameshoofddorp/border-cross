﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class WolffAudioManager : MonoBehaviour
    {
        public AudioClip spawnSoundEffect;
        public AudioClip explosionSoundEffect;
        public AudioClip deathSoundEffect1;
        public AudioClip deathSoundEffect2;
        public AudioClip deathSoundEffect3;
        public AudioClip deathSoundEffect4;
        public AudioClip deathSoundEffectWilhelm;
        public AudioClip shootGunSoundEffect;
        public AudioClip shootLaserSoundEffect;
        public AudioClip shootExplosionSoundEffect;
        public AudioClip winSound;
        public AudioClip defeatSound;
        public AudioClip bgMusicFaster;
        public AudioSource bgMusic;
        //public AudioSource bgMusicFast;
        public bool highSpeedNow = false;
        public int percentSpeedIncrease;

       //public AudioClip sound1;
       //public AudioClip sound2;
 
        //public AudioSource audio1;
        public AudioSource audio2;

        public AudioClip track1;
        public AudioClip track2;

        public float audio1Volume = 1.0f;
        public float audio2Volume = 0.0f;
        public bool track2Playing = false;

        public bool fadingOut = false;
        public bool fadingIn = false;
        public bool activateFadeInFadeOut = false;

        public bool defeated = false;
        public bool won = false;

        //public float v0;
        //public float t = 0;

        //private AudioSource audioSource;

        //public static AudioClip explosionStaticSoundEffect;
        //public static AudioClip deathStaticSoundEffect;

        //private static WolffAudioManager instance;

        // Use this for initialization
        void Start()
        {
            track1 = bgMusic.audio.clip;
            bgMusic.audio.loop = true;
            bgMusic.audio.Play();  
            /*audio1 = gameObject.GetComponent<AudioSource>();
            audio1.clip = sound1;
            audio1.Play();
            audio1.loop = true;*/
            audio2 = bgMusic.gameObject.AddComponent<AudioSource>();
            audio2.clip = track2;
            audio2.volume = 0;
            audio2.loop = true;
            audio2.rolloffMode = AudioRolloffMode.Linear;
            audio2.playOnAwake = false;
            //audio2.clip = sound2;
            //bgMusic = GetComponentInChildren<AudioSource>();
            //audioSource = GetComponentInChildren<AudioSource>();
            //audioSource.Play();
            //audioSource.loop = true;
            //audio.Play(backGroundMusic);
            //instance = this;
            //explosionStaticSoundEffect = explosionSoundEffect;
            //deathStaticSoundEffect = deathSoundEffect;
            //v0 = audio1.volume;
            //audio2.Play();
        }

        // Update is called once per frame
        void Update()
        {

            if (activateFadeInFadeOut == true)
            {
                fadingOut = true;
                fadingIn = true;
                activateFadeInFadeOut = false;
            }

            if(highSpeedNow == true)
            {
                //fadingOut = true;
                //fadingIn = true;
                GottaGoFaster();
                //CrossFade(audio1, audio2, 100f);
                highSpeedNow = false;
            }
            //CrossFade(audio1, audio2, 1000f);
            //float v0 = audio1.volume; // keep the original volume
            //audio2.Play(); // make sure a2 is playing
            //float t = 0;
            /*while (t < 1)
            {
                Debug.Log(t);
                t = Mathf.Clamp01(t + Time.deltaTime / 100f);
                audio1.volume = (1 - t) * v0;
                audio2.volume = t * v0;
                //yield;
                //return;
            }*/
            if (fadingOut == true)
            {
                fadeOut();
            }
 
     if (audio1Volume <= 0.8) {
         if(track2Playing == false)
         {
           track2Playing = true;
           //audio.clip = track2;
           if (defeated == true)
           {
               audio2.clip = defeatSound;
           }
           if (won == true)
           {
               audio2.clip = winSound;
           }
           audio2.Play();
         }
         if (fadingIn == true)
         {
             fadeIn();
         }
     }

        }

        public void PlaySoundEffect(AudioClip sound)
        {
            audio.PlayOneShot(sound);
            Debug.Log("sound played: " + sound);
        }

        public void PlayDeathSoundEffect()
        {
            int randomDeath = Random.Range(0, 9);
            switch (randomDeath)
            {
                case 0:
                    audio.PlayOneShot(deathSoundEffect1);
                    break;
                case 1:
                    audio.PlayOneShot(deathSoundEffect2);
                    break;
                case 2:
                    audio.PlayOneShot(deathSoundEffect3);
                    break;
                case 3:
                    audio.PlayOneShot(deathSoundEffect4);
                    break;
                case 4:
                    audio.PlayOneShot(deathSoundEffect1);
                    break;
                case 5:
                    audio.PlayOneShot(deathSoundEffect2);
                    break;
                case 6:
                    audio.PlayOneShot(deathSoundEffect3);
                    break;
                case 7:
                    audio.PlayOneShot(deathSoundEffect4);
                    break;
                case 8:
                    audio.PlayOneShot(deathSoundEffectWilhelm);
                    break;
            }


            Debug.Log("random death sound played: " + randomDeath);
        }

        public void PlayMusic(AudioClip sound)
        {
            bgMusic.clip = sound;
            //bgMusic.loop = true;
            //bgMusic.audio.PlayOneShot(sound);
            bgMusic.loop = true;
            bgMusic.Play();
            //audio.PlayOneShot(sound);
        }
        /*
        public void GottaGoFast()
        {
            bgMusic.clip = highSpeedBGMusic;
            bgMusic.loop = true;
            bgMusic.Play();
            bgMusic.mute = true;
            bgMusicFast.mute = false;
            bgMusicFast.time = 0.9091f * bgMusic.time;
        }*/

        public void GottaGoFaster()
        {
            /*bgMusic.clip = highSpeedBGMusic;
            bgMusic.loop = true;
            bgMusic.Play();*/
            var bgMusicTime = bgMusic.time;
            bgMusic.clip = bgMusicFaster;
            bgMusic.loop = true;
            bgMusic.Play();
            if (percentSpeedIncrease == 10)
            {
                bgMusic.time = 0.9091f * bgMusicTime;
            }
            else if (percentSpeedIncrease == 15)
            {
                bgMusic.time = 0.8696f * bgMusicTime;
            }
            else if (percentSpeedIncrease == 20)
            {
                bgMusic.time = 0.8333f * bgMusicTime;
            }
            else
            {
                bgMusic.time = bgMusicTime;
            }

            // crossfade sound2 to sound1 in 0.6 seconds
            //CrossFade(audio1, audio2, 0.6f);

        }

        public void fadeIn()
        {
            //audio2.clip = daNewMusic;
            if (audio2Volume < 1)
            {
                audio2Volume += 0.8f * Time.deltaTime;
                audio2.volume = audio2Volume;
            }
            else
            {
                fadingIn = false;
            }
        }

        public void fadeOut()
        {

            if (audio1Volume > 0)
            {
                audio1Volume -= 0.3f * Time.deltaTime;
                bgMusic.audio.volume = audio1Volume;
            }
            else
            {
                audio1Volume = 0;
                fadingOut = false;
            }
        }
        /*
        public void ReturnToNormalSpeed()
        {
            bgMusicFast.mute = true;
            bgMusic.mute = false; 
        }*/

        /*public static void PlayStaticSoundEffect(AudioClip soundStatic)
        {
            Debug.Log(soundStatic);
            instance.PlaySoundEffect(soundStatic);
            //audio.PlayOneShot(sound);
        }*/
 /*
        public void CrossFade(AudioSource a1, AudioSource a2, float duration){
            float v0 = a1.volume; // keep the original volume
            a2.Play(); // make sure a2 is playing
            float t = 0;
                while (t < 1){
                    //Debug.Log(t);
                    t = Mathf.Clamp01(t + Time.deltaTime / duration);
                    a1.volume = (1 - t) * v0;
                    a2.volume = t * v0;
                    //yield;
                    //return;
            }
        }*/
    }

}