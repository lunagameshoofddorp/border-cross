﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK
{
	
    public class LookAtCamera : MonoBehaviour {

        public GameObject aniRootObj;
        public Animator anim;
        //public AnimationState animState;

        //public Animation aniInstance;
        
        //public Transform turretObject;
		public Transform headObject;
        public Transform originalTransform;
        //public Transform headObject2;

        public bool lookAtCamera = false;
        //public bool inIdle = false;
        //public string currentState = "";

	// Use this for initialization
	void Start () {
        originalTransform = headObject.transform;
        anim = aniRootObj.GetComponent<Animator>();
        //animState = AnimationState
        //anim = aniRootObj.GetComponent<Animator>();
        //aniInstance = aniRootObj.GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {

        //Debug.Log("anim.name = " + anim.name);
        GetState();
        //if (anim.name == "Idle")
        //{
        //    lookAtCamera = true;
        //}
        //lookAtCamera = anim.GetCurrentAnimatorStateInfo(0).IsName("Idle");
        //Debug.Log("State = " + state);
        //bool inIdle = state.IsName("Locomotion.Idle");
        //currentState = aniInstance.IsPlaying("Idle").ToString;
        //if(aniInstance.IsPlaying("Idle"))
        //{
        //inIdle = true;
        //}
        //Debug.Log("inIdle = " + inIdle);
        //if (inIdle == true)
        //{
        //   lookAtCamera = true;
        //}
        if (lookAtCamera == true)
        {
            LookAtCameraPosition();
        }
        else
        {
            LookBack();
        }
        /*if (inIdle == false)
        {
            lookAtCamera = false;
        }*/
	}
        //Doesn't work yet
    public void LookBack()
    {
        headObject.transform.position = originalTransform.position;
        headObject.transform.rotation = originalTransform.rotation;
    }

    public void GetState()
    {
        lookAtCamera = anim.GetCurrentAnimatorStateInfo(0).IsName("Idle");
        //Debug.Log(lookAtCamera);
    }

    public void LookAtCameraPosition()
    {

        headObject.transform.LookAt(FindObjectOfType<Camera>().transform);
        //headObject2.transform.LookAt(FindObjectOfType<Camera>().transform);
				/*if(turretObject!=null){
                    if (headObject1 != null && headObject2 != null)
                    {
                        Vector3 targetPos = FindObjectOfType<Camera>().transform.position; //camera.transform.position;
                        Debug.Log(targetPos);
						Vector3 dummyPos=targetPos;
						dummyPos.y=turretObject.position.y;
						
						Quaternion wantedRot=Quaternion.LookRotation(dummyPos-turretObject.position);
						turretObject.rotation=Quaternion.Slerp(turretObject.rotation, wantedRot, Time.deltaTime);

                        float angle1 = Quaternion.LookRotation(targetPos - headObject1.position).eulerAngles.x;
                        float angle2 = Quaternion.LookRotation(targetPos - headObject2.position).eulerAngles.x;
						float distFactor=Mathf.Min(1, Vector3.Distance(turretObject.position, targetPos));
						float offset=distFactor;
						wantedRot=turretObject.rotation*Quaternion.Euler(angle1-offset, 0, 0);

                        headObject1.rotation = Quaternion.Slerp(headObject1.rotation, wantedRot, Time.deltaTime);
                        headObject2.rotation = Quaternion.Slerp(headObject2.rotation, wantedRot, Time.deltaTime);
						
						//if(Quaternion.Angle(barrelObject.rotation, wantedRot)<aimTolerance) targetInLOS=true;
						//else targetInLOS=false;
					}
                    else if (headObject1 != null)
                    {
                        Vector3 targetPos = FindObjectOfType<Camera>().transform.position; //camera.transform.position;
                        Debug.Log(targetPos);
                        Vector3 dummyPos = targetPos;
                        dummyPos.y = turretObject.position.y;

                        Quaternion wantedRot = Quaternion.LookRotation(dummyPos - turretObject.position);
                        turretObject.rotation = Quaternion.Slerp(turretObject.rotation, wantedRot, Time.deltaTime);

                        float angle = Quaternion.LookRotation(targetPos - headObject1.position).eulerAngles.x;
                        float distFactor = Mathf.Min(1, Vector3.Distance(turretObject.position, targetPos));
                        float offset = distFactor;
                        wantedRot = turretObject.rotation * Quaternion.Euler(angle - offset, 0, 0);

                        headObject1.rotation = Quaternion.Slerp(headObject1.rotation, wantedRot, Time.deltaTime);

                        //if(Quaternion.Angle(barrelObject.rotation, wantedRot)<aimTolerance) targetInLOS=true;
                        //else targetInLOS=false;
                    }
                    else
                    {
                        Vector3 targetPos = FindObjectOfType<Camera>().transform.position;
                        //if(!rotateTurretAimInXAxis) targetPos.y=turretObject.position.y;

                        Quaternion wantedRot = Quaternion.LookRotation(targetPos - turretObject.position);

                        float distFactor = Mathf.Min(1, Vector3.Distance(turretObject.position, targetPos));
                        float offset = distFactor;
                        wantedRot *= Quaternion.Euler(-offset, 0, 0);

                        turretObject.rotation = Quaternion.Slerp(turretObject.rotation, wantedRot, Time.deltaTime);

                        //if(Quaternion.Angle(turretObject.rotation, wantedRot)<aimTolerance) targetInLOS=true;
                        //else targetInLOS=false;
                    }
				}
				//else targetInLOS=true;*/
			}
			
			//rotate turret back to origin
			//if(IsCreep() && target==null && turretObject!=null && !stunned){
			//	turretObject.localRotation=Quaternion.Slerp(turretObject.localRotation, Quaternion.identity, turretRotateSpeed*Time.deltaTime*0.25f);
			}
			
		}