﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK
{

    public class FindDaPaths : MonoBehaviour
    {

        public PathTD Path1;
        public PathTD Path2;
        public PathTD Path3;

        public static PathTD PathStatic1;
        public static PathTD PathStatic2;
        public static PathTD PathStatic3;

        public GameObject PathPlatform1;
        public GameObject PathPlatform2;
        public GameObject PathPlatform3;

        public List<PlatformTD> Path1List = new List<PlatformTD>(0);
        public List<PlatformTD> Path2List = new List<PlatformTD>(0);
        public List<PlatformTD> Path3List = new List<PlatformTD>(0);

        // Use this for initialization
        void Start()
        {

            Path1 = GameObject.FindGameObjectWithTag("Path1").GetComponent<PathTD>();//GetComponent<PathTD>();
            Path2 = GameObject.FindGameObjectWithTag("Path2").GetComponent<PathTD>();
            Path3 = GameObject.FindGameObjectWithTag("Path3").GetComponent<PathTD>();
            PathPlatform1 = GameObject.FindGameObjectWithTag("PathPlatform1");
            PathPlatform2 = GameObject.FindGameObjectWithTag("PathPlatform2");
            PathPlatform3 = GameObject.FindGameObjectWithTag("PathPlatform3");
            PathStatic1 = Path1;
            PathStatic2 = Path2;
            PathStatic3 = Path3;

           /* foreach (Transform child in PathPlatform1.gameObject.transform)
            {
                Path1List.Add(child.gameObject.GetComponent<PlatformTD>());
            }

            foreach (Transform child in PathPlatform2.gameObject.transform)
            {
                Path2List.Add(child.gameObject.GetComponent<PlatformTD>());
            }

            foreach (Transform child in PathPlatform3.gameObject.transform)
            {
                Path3List.Add(child.gameObject.GetComponent<PlatformTD>());
            }*/
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}