﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class Trigger_UI_Pop_Up : MonoBehaviour
    {

        public int gameMessage;
        public UI_Pop_Up uiPopUp;

        // Use this for initialization
        void Start()
        {
            uiPopUp = FindObjectOfType<UI_Pop_Up>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnDestroy()
        {
            //var uiPopUp = FindObjectOfType<UI_Pop_Up>();
            Debug.Log("OnDestroy");
            uiPopUp._Show();
            //uiPopUp.gameMessage = 0;
            //uiPopUp.MessageInGameplay();
        }
    }

}