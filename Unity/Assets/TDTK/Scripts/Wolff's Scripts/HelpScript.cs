﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{
    public class HelpScript : MonoBehaviour
    {

        public string mainMenu;
        //public Texture2D texture;
        static HelpScript instance;

        void Awake()
        {
            instance = this;
            //gameObject.AddComponent<GUITexture>().enabled = true;
            //guiTexture.texture = texture;
            //transform.position = new Vector3(0.5f, 0.5f, 0.0f);
            //DontDestroyOnLoad(this); 
        }

        public void OnBackButton()
        {
            //Application.LoadLevel(mainMenu);
            PlayerPrefs.SetInt("firsttime", 0);
            LoadingScreenDynamic.Load(mainMenu);
        }

        public static void Load(int index)
        {
            if (NoInstance()) return;
            //instance.guiTexture.enabled = true;
            Application.LoadLevel(index);
            //instance.guiTexture.enabled = false;
        }

        public static void Load(string name)
        {
            instance.guiTexture.enabled = true;
            //Debug.Log("Load method entered");
            //if (NoInstance()) return;
            //Debug.Log("No return " + name);
            //if (FindObjectOfType<UI>() != null)
            //{
            //    var allUI = FindObjectOfType<UI>();
            //    allUI.gameObject.SetActive(false);
            //}
            Application.LoadLevel(name);
            //DontDestroyOnLoad(instance);
            /*if (Application.isLoadingLevel)
            {
                instance.guiTexture.enabled = true;
            }
            else
            {*/
            //instance.guiTexture.enabled = false;
            //}
        }

        static bool NoInstance()
        {
            if (!instance)
                Debug.LogError("Loading Screen is not existing in scene.");
            return instance;
        }
    }
}