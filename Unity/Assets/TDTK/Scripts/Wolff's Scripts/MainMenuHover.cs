﻿using UnityEngine;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class MainMenuHover : MonoBehaviour
    {

        public float Step; //A variable we increment
        public float Offset; //How far to offset the object upwards


        void Start()
        {
            //Store where we were placed in the editor
            var InitialPosition = transform.position;
            //Create an offset based on our height
            Offset = this.transform.localPosition.y;
        }

        void Update()
        {

            if (this.transform.localPosition.y < (Offset + 5f))
            {
                Step += 0.001f;
            }

          //Make sure Steps value never gets too out of hand 
            else
            {
                Step -= 0.001f;
            }

            //Float up and down along the y axis, 
            transform.Translate(0, Step * 0.005f, 0);

        }

    }
}