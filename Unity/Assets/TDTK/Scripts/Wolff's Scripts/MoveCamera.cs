﻿using UnityEngine;
using System.Collections;
using TDTK;

namespace TDTK
{

    public class MoveCamera : MonoBehaviour
    {

        public Vector3 thisPosition;
        public Transform target;
        public bool cameraMove = false;

        // Use this for initialization
        void Start()
        {
            thisPosition = this.gameObject.transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            if (cameraMove == true)
            {
                this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, target.position, 0.7f);
                if (this.gameObject.transform.position == target.position)
                {
                    cameraMove = false;
                }
            }
        }

        public void GOGOCamera()
        {
            cameraMove = true;
            var cameraControl = this.gameObject.GetComponentInParent<CameraControl>();
            cameraControl.enabled = false;
        }

    }

}