﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageSwap : MonoBehaviour {

    /*
    public bool rock = false;
    public bool paper = false;
    public bool scissor = false;
    */

    public int sprite = 0;
    public Sprite noUnit;
    public Sprite rockSprite;
    public Sprite paperSprite;
    public Sprite scissorprite;

    public Image[] images;

	// Use this for initialization
	void Start () {

            //spriteRenderer = GetComponent<SpriteRenderer>();
           // spriteRenderer.sprite = rockSprite;
        images = gameObject.GetComponentsInChildren<Image>();
	}
	
	// Update is called once per frame
	void Update () {

        switch (sprite)
        {
            case 0:
                NoUnitSprite();
                return;
            case 1:
                RockSprite();
                return;
            case 2:
                PaperSprite();
                return;
            case 3:
                ScissorSprite();
                return;
        }
    }

    public void NoUnitSprite()
    {
        foreach (Image image in images)
        {
            image.sprite = noUnit;
        }
    }

    public void RockSprite()
    {
        foreach (Image image in images)
        {
            image.sprite = rockSprite;
        }
    }

    public void PaperSprite()
    {
        foreach (Image image in images)
        {
            image.sprite = paperSprite;
        }   
    }

    public void ScissorSprite()
    {
        foreach (Image image in images)
        {
            image.sprite = scissorprite;
        }
    }

}
