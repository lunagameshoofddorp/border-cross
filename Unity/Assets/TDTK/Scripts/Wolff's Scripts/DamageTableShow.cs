﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using TDTK;

namespace TDTK
{

    public class DamageTableShow : MonoBehaviour
    {

        public Image[] images;

        public Sprite damageTable;
        public Sprite originalSprite;

        //public SpriteRenderer spriteRenderer;

        // Use this for initialization
        void OnAwake()
        {

        }

        void Start()
        {
            images = gameObject.GetComponentsInChildren<Image>();
            //spriteRenderer = GetComponent<SpriteRenderer>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ShowDamageTable()
        {
            Debug.Log("ShowDamageTable");
            //spriteRenderer.sprite = sprite;
            foreach (Image image in images)
            {
                image.sprite = damageTable;
            }
        }

        public void HideDamageTable()
        {
            Debug.Log("HideDamageTable");

            //gameObject.SetActive(false);
            //spriteRenderer.sprite = sprite;
            foreach (Image image in images)
            {
                image.sprite = originalSprite;
                //image.active = false;
            }
        }

    }

}