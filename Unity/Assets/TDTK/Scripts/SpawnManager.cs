﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK {

	public class SpawnManager : MonoBehaviour {
		
		public delegate void NewWaveHandler(int waveID);
		public static event NewWaveHandler onNewWaveE;
		
		public delegate void WaveSpawnedHandler(int time);
		public static event WaveSpawnedHandler onWaveSpawnedE;	//listen by TDTK
		
		public delegate void WaveClearedHandler(int time);
		public static event WaveClearedHandler onWaveClearedE;			//listen by TDTK
		
		public delegate void EnableSpawnHandler();
		public static event EnableSpawnHandler onEnableSpawnE;	//call to indicate it's ready to spawn next wave (when no longer in the process of actively spawning a wave)
		
		public delegate void SpawnTimerHandler(float time);
		public static event SpawnTimerHandler onSpawnTimerE;		//call to indicate timer refresh for continous spawn

        public static bool clicked = false;
        public bool waveCompleted = false;

        public GameObject currentUnit;
        public GameObject rock;
        public GameObject paper;
        public GameObject scissors;
        public GameObject scissor1;
        public GameObject scissor2;
        public GameObject scissor3;
        public GameObject scissor4;

        public static float showInterval = 0f;

        public bool showIntervalGo = false;
        public bool goTimer = false;


        public static string currentUnitName = "";
		
		
		public enum _SpawnMode{Continous, WaveCleared, Round}
		public _SpawnMode spawnMode;
		
		public enum _SpawnLimit{Finite, Infinite}
		public _SpawnLimit spawnLimit=_SpawnLimit.Finite;
		
		public bool allowSkip=false;
		
		public bool autoStart=false;
		public float autoStartDelay=5;
		public static bool AutoStart(){ return instance.autoStart; }
		public static float GetAutoStartDelay(){ return instance.autoStartDelay; }
		
		public bool procedurallyGenerateWave=false;	//used in finite mode, when enabled, all wave is generate procedurally

        public bool playerDefeated = false;
		
        public static PathTD defPath;
		public PathTD defaultPath;
        public PathTD path1;
        public PathTD path2;
        public PathTD path3;
        public static PathTD pathstatic1;
        public static PathTD pathstatic2;
        public static PathTD pathstatic3;
        
		private int currentWaveID=-1;			//start at -1, switch to 0 as soon as first wave start, always indicate latest spawned wave's ID
		public bool spawning=false;
		
		public int activeUnitCount=0;	//for wave-cleared mode checking
		public int totalSpawnCount=0;	//for creep instanceID
        public static int totalSpawnCount1 = 0;
		public int waveClearedCount=0; 	//for quick checking how many wave has been cleared
		
		public List<Wave> waveList=new List<Wave>();	//in endless mode, this is use to store temporary wave
		
		public WaveGenerator waveGenerator;
		
		public static SpawnManager instance;

        public static int WaveUnits =0;

        public int unitNR;
        public static int levelType;

        public WolffAudioManager audioManager;
        public bool gottaGoFast = true;
		
		void Awake(){
			instance=this;
		}
		
		// Use this for initialization
		void Start () {
            clicked = false;
            audioManager = FindObjectOfType<WolffAudioManager>();
            //levelType = Random.Range(0, 3);
            unitNR = 0;
            //Randomiser();
            LevelSpawner();
            path1 = FindDaPaths.PathStatic1;//.GetComponent<PathTD>();
            path2 = FindDaPaths.PathStatic2;//.GetComponent<PathTD>();
            path3 = FindDaPaths.PathStatic3;//.GetComponent<PathTD>();

			if(defaultPath==null){
				Debug.Log("DefaultPath on SpawnManager not assigned, auto search for one");
				//defaultPath=(PathTD)FindObjectOfType(typeof(PathTD));
			}
			
			if(spawnLimit==_SpawnLimit.Infinite || procedurallyGenerateWave){
				waveGenerator.CheckPathList();
				if(defaultPath!=null && waveGenerator.pathList.Count==0) waveGenerator.pathList.Add(defaultPath);
			}
			
			//waveGenerator.Generate(i);
			if(spawnLimit==_SpawnLimit.Finite && procedurallyGenerateWave){
				for(int i=0; i<waveList.Count; i++){
					waveList[i]=waveGenerator.Generate(i);
				}
			}
			
			if(spawnLimit==_SpawnLimit.Infinite) waveList=new List<Wave>();
			if(spawnLimit==_SpawnLimit.Finite){
				for(int i=0; i<waveList.Count; i++) waveList[i].waveID=i;
			}
			
			if(autoStart) StartCoroutine(AutoStartRoutine());
		}

        void Update()
        {
            

            if(showIntervalGo == true)
            {
                showInterval = 2.8f;
                goTimer = true;
                showIntervalGo = false;
            }

                if (goTimer == true)
                {
                    showInterval -= Time.deltaTime;
                }
            
            if (showInterval < 0)
            {    
                goTimer = false;
            }

            defPath = defaultPath;
            pathstatic1 = path1;
            pathstatic2 = path2;
            pathstatic3 = path3;
            CheckPlayerPath();
            try
            {
                if (currentUnit == rock)
                {
                    currentUnitName = "rock";
                    var imageSwap = FindObjectOfType<ImageSwap>();
                    imageSwap.sprite = 1;
                }
                else if (currentUnit == paper)
                {
                    currentUnitName = "paper";
                    var imageSwap = FindObjectOfType<ImageSwap>();
                    imageSwap.sprite = 2;
                }
                else if (currentUnit == scissors)
                {
                    currentUnitName = "scissor";
                    var imageSwap = FindObjectOfType<ImageSwap>();
                    imageSwap.sprite = 3;
                }
            }
            catch
            {

            }
            if (totalSpawnCount == WaveUnits && activeUnitCount <= 0 && GameControl.IsGameStarted() == true)
            {
                playerDefeated = true;
            }
        }
		
		
		IEnumerator AutoStartRoutine(){
			yield return new WaitForSeconds(autoStartDelay);
			_Spawn();
		}
		
		
		void OnEnable(){
			Unit.onDestroyedE += OnUnitDestroyed;
			UnitCreep.onDestinationE += OnUnitReachDestination;
		}
		void OnDisable(){
			Unit.onDestroyedE -= OnUnitDestroyed;
			UnitCreep.onDestinationE -= OnUnitReachDestination;
		}
		
		
		void OnUnitDestroyed(Unit unit){
			if(!unit.IsCreep()) return;
			
			UnitCreep creep=unit.GetUnitCreep();
			OnUnitCleared(creep);
		}
		void OnUnitReachDestination(UnitCreep creep){
			//only execute if creep is dead 
			//when using path-looping the creep would be still active and wouldnt set it's dead flag to true
			if(creep.dead) OnUnitCleared(creep);
		}
		void OnUnitCleared(UnitCreep creep){
			int waveID=creep.waveID;
			
			activeUnitCount-=1;
			
			Wave wave=null;
			if(spawnLimit==_SpawnLimit.Finite) wave=waveList[waveID];
			else if(spawnLimit==_SpawnLimit.Infinite){
				for(int i=0; i<waveList.Count; i++){
					if(waveList[i].waveID==waveID){
						wave=waveList[i];
						break;
					}
				}
				
				if(wave==null){
					Debug.Log("error!");
					return;
				}
			}
			
			
			wave.activeUnitCount-=1;
			if(wave.spawned && wave.activeUnitCount==0){
				wave.cleared=true;
				waveClearedCount+=1;
				Debug.Log("wave"+(waveID+1)+ " is cleared");
				
				ResourceManager.GainResource(wave.rscGainList, PerkManager.GetRscWaveKilled());
				GameControl.GainLife(wave.lifeGain+PerkManager.GetLifeWaveClearedModifier());
				AbilityManager.GainEnergy(wave.energyGain+(int)PerkManager.GetEnergyWaveClearedModifier());
				
				if(spawnLimit==_SpawnLimit.Infinite) waveList.Remove(wave);
				
				if(IsAllWaveCleared()){
					GameControl.GameWon();
				}
				else{
					if(spawnMode==_SpawnMode.Round && onEnableSpawnE!=null) onEnableSpawnE();
				}
			}
			
			
			if(!IsAllWaveCleared() && activeUnitCount==0 && !spawning){
				if(spawnMode==_SpawnMode.WaveCleared) SpawnWaveFinite();
			}
			
		}
		
		
		public static int AddDestroyedSpawn(UnitCreep unit){ return instance._AddDestroyedSpawn(unit); }
		public int _AddDestroyedSpawn(UnitCreep unit){
			activeUnitCount+=1;
			
			if(spawnLimit==_SpawnLimit.Finite) waveList[unit.waveID].activeUnitCount+=1;
			else if(spawnLimit==_SpawnLimit.Infinite){
				for(int i=0; i<waveList.Count; i++){
					if(waveList[i].waveID==unit.waveID){
						waveList[i].activeUnitCount+=1;
						break;
					}
				}
			}
            totalSpawnCount1 += 1;
			return totalSpawnCount+=1;
		}
		
		
		
		
		public static void Spawn(){ instance._Spawn(); }
		public void _Spawn(){
			if(GameControl.IsGameOver()) return;
			if(IsSpawningStarted()){
				if(spawnMode==_SpawnMode.Round){
					if(!waveList[currentWaveID].cleared) return;
				}
				else if(!allowSkip) return;
				
				SpawnWaveFinite();
				
				return;
			}
			
			if(spawnMode!=_SpawnMode.Continous) SpawnWaveFinite();
			else StartCoroutine(ContinousSpawnRoutine());
			
			//spawningStarted=true;
			GameControl.StartGame();
		}
		
		IEnumerator ContinousSpawnRoutine(){
			while(true){
				if(GameControl.IsGameOver()) yield break;
				
				float duration=SpawnWaveFinite();
				if(spawnLimit==_SpawnLimit.Finite && currentWaveID>=waveList.Count) break;
				yield return new WaitForSeconds(duration);
			}
		}
		
		
		private float SpawnWaveFinite(){

			if(spawning) return 0;

            

            totalSpawnCount1 = 0;

            spawning =true;
			currentWaveID+=1;
			
			if(spawnLimit==_SpawnLimit.Finite && currentWaveID>=waveList.Count) return 0;
            
            WaveUnits = 0;
                for (int i = 0; i < waveList[currentWaveID].subWaveList.Count; i++)
                {
                    WaveUnits += waveList[currentWaveID].subWaveList[i].count;
                }
			Debug.Log("spawning wave"+(currentWaveID+1));
            Debug.Log(waveList[currentWaveID].subWaveList[0].count);
			if(onNewWaveE!=null) onNewWaveE(currentWaveID+1);
			
			Wave wave=null;
			if(spawnLimit==_SpawnLimit.Finite) wave=waveList[currentWaveID];
			else if(spawnLimit==_SpawnLimit.Infinite){
				wave=waveGenerator.Generate(currentWaveID);
				wave.waveID=currentWaveID;
				waveList.Add(wave);
			}
			
			if(spawnMode==_SpawnMode.Continous){
				if(spawnLimit==_SpawnLimit.Finite && currentWaveID<waveList.Count-1){
					if(onSpawnTimerE!=null) onSpawnTimerE(wave.duration);
				}
				else if(spawnLimit==_SpawnLimit.Infinite){
					if(onSpawnTimerE!=null) onSpawnTimerE(wave.duration);
				}
			}
			
			for(int i=0; i<wave.subWaveList.Count; i++){
				StartCoroutine(SpawnSubWave(wave.subWaveList[i], wave));
                string unitName = wave.subWaveList[i].unit.name;
                if (unitName == "CreepTank1")
                //if (unit.name == "Vehicle")
                {
                    //Debug.Log(unit.unitName);
                    var imageSwap = FindObjectOfType<ImageSwap>();
                    imageSwap.sprite = 1;
                }
                if (unitName == "CreepTank2")
                //if (unit.unitName == "Freighter I")
                {
                    //Debug.Log(unit.unitName);
                    var imageSwap = FindObjectOfType<ImageSwap>();
                    imageSwap.sprite = 2;
                }
                if (unitName == "CreepMissile")
                //if (unit.unitName == "Drone I")
                {
                    //Debug.Log(unit.unitName);
                    var imageSwap = FindObjectOfType<ImageSwap>();
                    imageSwap.sprite = 3;
                }
			}
            
			
			return wave.duration;
		}
        IEnumerator SpawnSubWave(SubWave subWave, Wave parentWave)
        {

            while (waveCompleted == false)
            {
                /*
                if (subWave.unit.name == "CreepVehicle")
                //if (unit.name == "Vehicle")
                {
                    //Debug.Log(unit.unitName);
                    var imageSwap = FindObjectOfType<ImageSwap>();
                    imageSwap.sprite = 1;
                }
                if (subWave.unit.name == "CreepFreighter1")
                //if (unit.unitName == "Freighter I")
                {
                    //Debug.Log(unit.unitName);
                    var imageSwap = FindObjectOfType<ImageSwap>();
                    imageSwap.sprite = 2;
                }
                if (subWave.unit.name == "CreepDrone1")
                //if (unit.unitName == "Drone I")
                {
                    //Debug.Log(unit.unitName);
                    var imageSwap = FindObjectOfType<ImageSwap>();
                    imageSwap.sprite = 3;
                }*/

                yield return new WaitForSeconds(subWave.delay);

                PathTD path = defaultPath;
                if (subWave.path != null) subWave.path = defaultPath;

                //Vector3 pos=path.GetSpawnPoint().position;
                //Quaternion rot=path.GetSpawnPoint().rotation;

                int spawnCount = 0;
                if (totalSpawnCount < WaveUnits)
                {
                    while (clicked)
                    {
                        if (spawnCount < subWave.count)
                        {
                            Vector3 pos = defaultPath.GetSpawnPoint().position;
                            Quaternion rot = defaultPath.GetSpawnPoint().rotation;
                            GameObject obj;
                            /*GameObject obj2 = scissor2;
                            GameObject obj3 = scissor3;
                            GameObject obj4 = scissor4;*/
                            UnitCreep unit = null;
                            /*UnitCreep unit2 = null;
                            UnitCreep unit3 = null;
                            UnitCreep unit4 = null;*/
                            /*if (currentUnit == scissors)
                            {
                                obj2 = ObjectPoolManager.Spawn(scissor2, pos, rot);
                                unit2 = obj2.GetComponent<UnitCreep>();
                                unit2.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                                yield return new WaitForSeconds(0.3f);
                                obj = ObjectPoolManager.Spawn(scissor1, pos, rot);
                                unit = obj.GetComponent<UnitCreep>();
                                unit.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                                yield return new WaitForSeconds(0.65f);
                                obj3 = ObjectPoolManager.Spawn(scissor3, pos, rot);
                                unit3 = obj3.GetComponent<UnitCreep>();
                                unit3.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                                yield return new WaitForSeconds(0.4f);
                                obj4 = ObjectPoolManager.Spawn(scissor4, pos, rot);
                                unit4 = obj4.GetComponent<UnitCreep>();
                                unit4.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                                activeUnitCount += 4;
                            }
                            else
                            {*/
                                obj = ObjectPoolManager.Spawn(currentUnit, pos, rot); //subWave.unit edited to gameObject
                                unit = obj.GetComponent<UnitCreep>();
                                unit.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                                activeUnitCount += 1;
                                if (gottaGoFast == true)
                                {
                                    audioManager.GottaGoFaster();
                                    gottaGoFast = false;
                                }
                                audioManager.PlaySoundEffect(audioManager.spawnSoundEffect);
                            
                            //}

                            //UnitCreep unit = obj.GetComponent<UnitCreep>();

                            if (subWave.overrideHP > 0) unit.defaultHP = subWave.overrideHP;
                            if (subWave.overrideShield > 0) unit.defaultShield = subWave.overrideShield;
                            if (subWave.overrideMoveSpd > 0) unit.moveSpeed = subWave.overrideMoveSpd;

                            //unit.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                            /*
                                if (currentUnit == scissors)
                                {
                                    //StartCoroutine(_SpawnDelay(0.2f));
                                    yield return new WaitForSeconds(0.3f);
                                    unit2 = obj2.GetComponent<UnitCreep>();
                                    unit2.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                                    yield return new WaitForSeconds(0.3f);
                                    unit3 = obj3.GetComponent<UnitCreep>();
                                    unit3.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                                    yield return new WaitForSeconds(0.3f);
                                    unit4 = obj4.GetComponent<UnitCreep>();
                                    unit4.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                                }
                            */


                            //if (currentUnit == scissors)
                            //{
                            //    unit2.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                            //    unit3.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                            //    unit4.Init(defaultPath, totalSpawnCount, parentWave.waveID);
                            //}



                            if (currentUnitName == "rock")
                            {
                                defaultPath.GetComponentInChildren<MouseClicked>().rockThreat++;
                            }
                            if (currentUnitName == "paper")
                            {
                                defaultPath.GetComponentInChildren<MouseClicked>().paperThreat++;
                            }
                            if (currentUnitName == "scissor")
                            {
                                defaultPath.GetComponentInChildren<MouseClicked>().scissorThreat++;
                            }

                            Debug.Log(subWave.unit.name);
                            Debug.Log(unit.unitName);
                            Debug.Log("defaultPath = " + defaultPath);
                            Debug.Log("defaultPathRockThreath = " + defaultPath.GetComponentInChildren<MouseClicked>().rockThreat);
                            Debug.Log("defaultPathPaperThreat = " + defaultPath.GetComponentInChildren<MouseClicked>().paperThreat);
                            Debug.Log("defaultPathScissorThreat = " + defaultPath.GetComponentInChildren<MouseClicked>().scissorThreat);
                            totalSpawnCount1 += 1;
                            totalSpawnCount += 1;
                            //activeUnitCount += 1;

                            parentWave.activeUnitCount += 1;

                            spawnCount += 1;

                            if (spawnCount == subWave.count)
                            {
                                RemoveArrows();
                                break;
                            }
                            LevelSpawner();
                            //Randomiser();
                            //AI.canBuild++;
                            //NewAI.buildNumber++;
                            clicked = false;
                            if (currentUnit != null)
                            {
                                showIntervalGo = true;
                            }
                            yield return new WaitForSeconds(subWave.interval);
                            clicked = false;

                        }
                    }
                }
                /*
                parentWave.subWaveSpawnedCount += 1;
            
                if(parentWave.subWaveSpawnedCount==parentWave.subWaveList.Count){
                    parentWave.spawned=true;
                    spawning=false;
                    Debug.Log("wave "+(parentWave.waveID+1)+" has done spawning");
				
                    yield return new WaitForSeconds(0.5f);
				
                    if(currentWaveID<=waveList.Count-2){
                        //for UI to show spawn button again
                        if(spawnMode==_SpawnMode.Continous && allowSkip && onEnableSpawnE!=null) onEnableSpawnE();
                        if(spawnMode==_SpawnMode.WaveCleared && allowSkip && onEnableSpawnE!=null) onEnableSpawnE();
                    }
                }*/
                if (spawnCount == subWave.count)
                {
                    waveCompleted = true;
                    clicked = false;
                }
            }
        }


        //Debug.Log(waveList[0].subWaveList[0].count);

        public void LevelSpawner()
        {
            switch (GameControl.GetLevelID())
            {
                case 0:
                    SpawnLevel0();
                    break;
                case 1:
                    SpawnLevel1();
                    break;
                case 2:
                    SpawnLevel2();
                    break;
                case 3:
                    //level order changed to reflect difficulty build-up.
                    SpawnLevel3();
                    break;
                case 4:
                    SpawnLevel4();
                    break;
                case 5:
                    SpawnLevel5();
                    break;
                case 6:
                    SpawnLevel6();
                    break;
                case 7:
                    SpawnLevel7();
                    break;
                case 8:
                    SpawnLevel8();
                    break;
                case 9:
                    SpawnLevel9();
                    break;
            }
        }

        /*
        public void SpawnTutorial()
    {
                switch(unitNR)
                {
                    case 1:
                        SelectTutorialPart();
                        break;
                    case 2:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        //FindObjectOfType<ImageSwap>().NoUnitSprite();
                        break;
                }
    }
        
        public void SelectTutorialPart()
        {
            switch (TutorialManager.staticTutorialPart)
            {
                case 1:
                    DoTutorial1();
                    break;
                case 2:
                    DoTutorial2();
                    break;
                case 3:
                    DoTutorial3();
                    break;
                case 4:
                    DoTutorial4();
                    break;
            }
        }

        public void DoTutorial1()
        {
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = rock;
                    break;
                case 2:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }

        public void DoTutorial2()
        {
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = rock;
                    break;
                case 2:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }

        public void DoTutorial3()
        {
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = rock;
                    break;
                case 2:
                    currentUnit = scissors;
                    break;
                case 3:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }

        public void DoTutorial4()
        {
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = rock;
                    break;
                case 2:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }*/

        //scissor, rock, paper

        public void SpawnLevel0()
        {
            Debug.Log("leveltype = " + levelType);
                unitNR++;
                switch (unitNR)
                {
                    case 1:
                        currentUnit = rock;
                        break;
                    case 2:
                        RemoveArrows();
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
        }

        public void SpawnLevel1()
        {
            Debug.Log("leveltype = " + levelType);
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = rock;
                    break;
                case 2:
                    RemoveArrows();
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }

        public void SpawnLevel2()
        {
            Debug.Log("leveltype = " + levelType);
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = rock;
                    break;
                case 2:
                    currentUnit = scissors;
                    break;
                case 3:
                    RemoveArrows();
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }

        public void SpawnLevel3()
        {
            Debug.Log("leveltype = " + levelType);
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = paper;
                    break;
                case 2:
                    currentUnit = rock;
                    break;
                case 3:
                    currentUnit = scissors;
                    break;
                case 4:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }

        public void SpawnLevel4()
        {
            Debug.Log("leveltype = " + levelType);
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = scissors;
                    break;
                case 2:
                    currentUnit = rock;
                    break;
                case 3:
                    currentUnit = paper;
                    break;
                case 4:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }

        public void SpawnLevel5()
        {
            Debug.Log("leveltype = " + levelType);
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = rock;
                    break;
                case 2:
                    currentUnit = paper;
                    break;
                case 3:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }

        public void SpawnLevel6()
        {
            Debug.Log("leveltype = " + levelType);
            if (levelType == 0)
            {
                unitNR++;
                switch (unitNR)
                {
                    case 1:
                        currentUnit = paper;
                        break;
                    case 2:
                        currentUnit = rock;
                        break;
                    case 3:
                        currentUnit = scissors;
                        break;
                    case 4:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
            }
            if (levelType == 1)
            {
                unitNR++;
                switch (unitNR)
                {
                    case 1:
                        currentUnit = paper;
                        break;
                    case 2:
                        currentUnit = scissors;
                        break;
                    case 3:
                        currentUnit = rock;
                        break;
                    case 4:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
            }
            if (levelType == 2)
            {
                unitNR++;
                switch (unitNR)
                {
                    case 1:
                        currentUnit = paper;
                        break;
                    case 2:
                        currentUnit = rock;
                        break;
                    case 3:
                        currentUnit = scissors;
                        break;
                    case 4:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
            }
        }

        public void SpawnLevel7()
        {
            Debug.Log("leveltype = " + levelType);

            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = scissors;
                    break;
                case 2:
                    currentUnit = paper;
                    break;
                case 3:
                    currentUnit = rock;
                    break;
                case 4:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }    
        }

        

        public void SpawnLevel8()
        {
            Debug.Log("leveltype = " + levelType);
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = scissors;
                    break;
                case 2:
                    currentUnit = rock;
                    break;
                case 3:
                    currentUnit = paper;
                    break;
                case 4:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }

        public void SpawnLevel9()
        {
            unitNR++;
            if (levelType == 0 || levelType == 2)
            {
                switch (unitNR)
                {
                    case 1:
                        currentUnit = paper;
                        break;
                    case 2:
                        currentUnit = rock;
                        break;
                    case 3:
                        currentUnit = scissors;
                        break;
                    case 4:
                        currentUnit = paper;
                        break;
                    case 5:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
            }
            else if (levelType == 1)
            {
                switch (unitNR)
                {
                    case 1:
                        currentUnit = paper;
                        break;
                    case 2:
                        currentUnit = rock;
                        break;
                    case 3:
                        currentUnit = scissors;
                        break;
                    case 4:
                        currentUnit = rock;
                        break;
                    case 5:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
            }
        }

        

        /*
        public void SpawnLevel1()
        {
            Debug.Log("leveltype = " + levelType);
            if (levelType == 0)
            {
            unitNR++;
                switch(unitNR)
                {
                    case 1:
                        currentUnit = paper;
                        break;
                    case 2:
                        currentUnit = rock;
                        break;
                    case 3:
                        currentUnit = scissors;
                        break;
                    case 4:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
            }
            if (levelType == 1)
            {
                unitNR++;
                switch (unitNR)
                {
                    case 1:
                        currentUnit = paper;
                        break;
                    case 2:
                        currentUnit = scissors;
                        break;
                    case 3:
                        currentUnit = rock;
                        break;
                    case 4:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
            }
            if (levelType == 2)
            {
                unitNR++;
                switch (unitNR)
                {
                    case 1:
                        currentUnit = paper;
                        break;
                    case 2:
                        currentUnit = rock;
                        break;
                    case 3:
                        currentUnit = scissors;
                        break;
                    case 4:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
            }
        }

        public void SpawnLevel2()
        {
            Debug.Log("leveltype = " + levelType);
           
                unitNR++;
                switch (unitNR)
                {
                    case 1:
                        currentUnit = scissors;
                        break;
                    case 2:
                        currentUnit = paper;
                        break;
                    case 3:
                        currentUnit = rock;
                        break;
                    case 4:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }    
        }

        public void SpawnLevel3()
        {
            Debug.Log("leveltype = " + levelType);
                unitNR++;
                switch (unitNR)
                {
                    case 1:
                        currentUnit = rock;
                        break;
                    case 2:
                        currentUnit = paper;
                        break;
                    case 3:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
            }


        public void SpawnLevel4()
        {
                unitNR++;
                switch (unitNR)
                {
                    case 1:
                        currentUnit = scissors;
                        break;
                    case 2:
                        currentUnit = rock;
                        break;
                    case 3:
                        currentUnit = null;
                        FindObjectOfType<ImageSwap>().sprite = 0;
                        break;
                }
        }

        public void SpawnLevel5()
        {
            unitNR++;
            switch (unitNR)
            {
                case 1:
                    currentUnit = paper;
                    break;
                case 2:
                    currentUnit = rock;
                    break;
                case 3:
                    currentUnit = scissors;
                    break;
                case 4:
                    currentUnit = paper;
                    break;
                case 5:
                    currentUnit = null;
                    FindObjectOfType<ImageSwap>().sprite = 0;
                    break;
            }
        }*/

        public void RemoveArrows()
        {
            try
            {
                var arrows = FindObjectsOfType<ArrowHover>();


                foreach (ArrowHover arrow in arrows)
                {
                    arrow.gameObject.SetActive(false);
                }
            }
            catch
            {
                Debug.Log("No Arrows");
            }
        }

        public void Randomiser()
        {
            /*
            int randomUnit = Random.Range(0, 3);
            if (randomUnit == 0)
            {
                currentUnit = rock;
            }
            if (randomUnit == 1)
            {
                currentUnit = paper;
            }
            if (randomUnit == 2)
            {
                currentUnit = scissors;
            }*/
            if (currentUnit == null)
            {
                int randomUnit = Random.Range(0, 3);
                if (randomUnit == 0)
                {
                    currentUnit = rock;
                    return;
                }
                if (randomUnit == 1)
                {
                    currentUnit = paper;
                    return;
                }
                if (randomUnit == 2)
                {
                    currentUnit = scissors;
                    return;
                }
            }

            if (currentUnit == rock)
            {
                int randomUnit = Random.Range(0, 2);
                if (randomUnit == 0)
                {
                    currentUnit = paper;
                    return;
                }
                if (randomUnit == 1)
                {
                    currentUnit = scissors;
                    return;
                }
            }

            if (currentUnit == paper)
            {
                int randomUnit = Random.Range(0, 2);
                if (randomUnit == 0)
                {
                    currentUnit = rock;
                    return;
                }
                if (randomUnit == 1)
                {
                    currentUnit = scissors;
                    return;
                }
            }

            if (currentUnit == scissors)
            {
                int randomUnit = Random.Range(0, 2);
                if (randomUnit == 0)
                {
                    currentUnit = rock;
                    return;
                }
                if (randomUnit == 1)
                {
                    currentUnit = paper;
                    return;
                }
            }
        }

        public void CheckPlayerPath()
        {
            waveGenerator.CheckPathList();
            if (defaultPath != null) waveGenerator.pathList.Add(defaultPath);
        }
		
		public bool IsSpawningStarted(){
			return currentWaveID>=0 ? true : false;
		}
		
		public static bool IsAllWaveCleared(){
			if(instance.spawnLimit==_SpawnLimit.Infinite) return false;
			
			//Debug.Log("check all wave cleared   "+instance.waveClearedCount+"   "+instance.waveList.Count);
			if(instance.waveClearedCount>=instance.waveList.Count) return true;
			return false;
		}
		
		public static int GetTotalWaveCount(){
			if(instance.spawnLimit==_SpawnLimit.Infinite) return -1;
			return instance.waveList.Count;
		}
		
		public static int GetCurrentWaveID(){ return instance.currentWaveID; }
	}

}