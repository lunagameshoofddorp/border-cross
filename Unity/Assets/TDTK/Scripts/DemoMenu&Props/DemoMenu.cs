﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK {

	public class DemoMenu : MonoBehaviour {

		public RectTransform frame;
		
		public List<string> displayedName=new List<string>();
		public List<string> levelName=new List<string>();
		public List<UnityButton> buttonList=new List<UnityButton>();
        //public LoadingScreenDynamic levelExtraTimeLoader;

        public string helpScreen;
        public int j = 0;

        //public GameObject mainMenuObj;
        //public GameObject helpMenuObj;
		
		// Use this for initialization
		void Start (){
            //if(helpMenuObj != null)
            //{
            //    StartOnBackButton();
            //}

            Debug.Log("firsttime main menu " + PlayerPrefs.GetInt("firsttime"));
            PlayerPrefs.SetInt("firsttime", 1);

			for(int i=0; i<levelName.Count; i++){
				if(i==0) buttonList[0].Init();
				else if(i>0 && i < 5){
                    //buttonList.Add(buttonList[0].Clone("ButtonStart" + (i + 1), new Vector3(i * 120, 0, 0)));
					UnityButton b = buttonList[0].Clone("ButtonStart"+(i+1), new Vector3(i * 120, 0, 0));
                    buttonList.Add(b);
                    LevelUI lU = b.rootObj.GetComponent<LevelUI>();
                    lU.levelNumber = i;
				}
                else if(i > 0 && i < 10)
                {
                    //buttonList.Add(buttonList[0].Clone("ButtonStart" + (i + 1), new Vector3(j * 120, -60, 0)));
                    UnityButton b = buttonList[0].Clone("ButtonStart" + (i + 1), new Vector3(j * 120, -60, 0));
                    buttonList.Add(b);
                    LevelUI lU = b.rootObj.GetComponent<LevelUI>();
                    lU.levelNumber = i;
                    j++;
                }
				
				buttonList[i].label.text=displayedName[i];
			}

            //levelExtraTimeLoader = FindObjectOfType<LoadingScreenDynamic>();
			
			frame.sizeDelta=new Vector2(200, 30+levelName.Count*40);

            //Debug.Log("Firsttime is now " + PlayerPrefs.GetInt("firsttime"));
		}
		
		// Update is called once per frame
		void Update () {
			if (Input.GetKeyDown(KeyCode.Escape)) { 
				Application.Quit(); 
				//Debug.Log ("End app");
			}
            //Debug.Log("Firsttime is now " + PlayerPrefs.GetInt("firsttime"));
		}
		
		public void OnStartButton(GameObject butObj){
			for(int i=0; i<buttonList.Count; i++){
				if(buttonList[i].rootObj==butObj){
					//Application.LoadLevel(levelName[i]);
                    if(levelName[i] == "TutorialPart1(0)" || levelName[i] == "TutorialPart2(1)" || levelName[i] == "TutorialPart3(2)")
                    {
                        //levelExtraTimeLoader.LoadExtraTime(levelName[i]);
                        LoadingScreenDynamic.LoadWithExtraTime((levelName[i]));
                    }
                    else
                    {
                    LoadingScreenDynamic.Load(levelName[i]);
                    }
				}
			}
		}

        public void OnHelpButton()
        {
            //mainMenuObj.SetActive(false);
            //helpMenuObj.SetActive(true);
            //FindObjectOfType<DamageTableShow>().ShowDamageTable();

            //Application.LoadLevel(helpScreen);
            LoadingScreenDynamic.Load(helpScreen);
        }

        /*public void OnResetButton()
        {
            LevelManager.ReLockLevels();
            LoadingScreenDynamic.Load(Application.loadedLevelName);
        }*/

        /*
        public void StartOnBackButton()
        {
            mainMenuObj.SetActive(true);
            helpMenuObj.SetActive(false);
            
        }

        public void OnBackButton()
        {
            FindObjectOfType<DamageTableShow>().HideDamageTable();
            mainMenuObj.SetActive(true);
            helpMenuObj.SetActive(false);     
        }*/
		
	}

}