﻿using UnityEngine;
using System.Collections;

public class AdManager : MonoBehaviour {

	public bool showAdvert = true;
	public bool cacheAdvert = false;

	// Use this for initialization
	void Start () {

		if (HDApi.instance != null) {

			if (cacheAdvert) {
				HDApi.instance.CheckForAd();
			}
			if (showAdvert) {
				HDApi.instance.ShowAd();
			}
		}
	}
	
	public void CacheRewardedAd() {

		HDApi.instance.CheckForRewardedAd ();
	}

	public void ShowRewardedAd() {

		HDApi.instance.ShowRewardedAd ();
	}
}
