﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.ComponentModel;
using Prime31.WinPhoneAdMob;


public class HDApi : MonoBehaviour {

	public string mainMenu = "";
	public string lunastreamsID = "";
	public string androidRewardedZone = "";
	public string iOSRewardedZone = "";
	public string wpAdMob = "";
	private bool isInitDone = false;
	private bool isAdAvailable = false;
	private Dictionary<string, string> _settings = null;
	private int firstTimeLoaded = 0;

	public static HDApi instance = null;

	void Awake()
	{
		Debug.Log ("FT - reg events");

		FuseAPI.SessionStartReceived += SessionStarted;
		FuseAPI.AdAvailabilityResponse += AdAvailabilityResponse;
		FuseAPI.AdWillClose += AdWillClose;
		FuseAPI.SessionLoginError += SessionLoginError;
		FuseAPI.RewardedVideoCompleted += RewardedVideoCompleted;
#if UNITY_WP8
		WinPhoneAdMob.interstitialReceivedAdEvent += () =>
		{
			isAdAvailable = true;
			Debug.Log("AD FOUND!");
		};
		
		WinPhoneAdMob.interstitialFailedToReceiveAdEvent += ( error ) =>
		{
			Debug.Log("ad NOT found!");
		};
#endif

		instance = this;
		DontDestroyOnLoad (this);
	}

	void Start() {
		Debug.Log ("firsttime " + PlayerPrefs.GetInt("firsttime"));
		firstTimeLoaded = PlayerPrefs.GetInt("firsttime");
		//Debug.Log("firsttime");
		StartCoroutine (InitLunastreams (lunastreamsID));
		StartCoroutine (InitAndLoadNext ());
	}

	void OnDestroy()
	{
		Debug.Log ("FT - UNreg events");

		FuseAPI.SessionStartReceived -= SessionStarted;
		FuseAPI.AdAvailabilityResponse -= AdAvailabilityResponse;
		FuseAPI.AdWillClose -= AdWillClose;
		FuseAPI.SessionLoginError -= SessionLoginError;
		FuseAPI.RewardedVideoCompleted -= RewardedVideoCompleted;

	}


	private IEnumerator InitAndLoadNext()
	{
#if UNITY_WP8
		isInitDone = true;
		CheckForAd ();
#endif

		if(firstTimeLoaded == 0)
		{
			Application.LoadLevel (mainMenu);

			Debug.Log ("Firsttime = " + PlayerPrefs.GetInt("firsttime"));

            //Debug.Log("(!= 0) Firsttime is now " + PlayerPrefs.GetInt("firsttime"));

			yield return null;
		}


		if(firstTimeLoaded != 0)
		{
			//Debug.Log (PlayerPrefs.GetInt("firsttime " + PlayerPrefs.GetInt("firsttime")));
		
			int tries = 3;
			while (!isAdAvailable && tries > 0) {
				Debug.Log ("FT - waiting...");
				yield return new WaitForSeconds (1);
				tries--;

				Debug.Log("I DONT WANT THIS");
                Debug.Log("Firsttime is now " + PlayerPrefs.GetInt("firsttime"));

				Application.LoadLevel (mainMenu);
				yield return null;

			}
		}
	}


	private void SessionStarted()
	{
		Debug.Log ("FT - Start Session!");
		isInitDone = true;
		StartCoroutine(WaitAndCheckAd());
	}
	
	private IEnumerator WaitAndCheckAd()
	{
		//yield return new WaitForSeconds(1);
		//Debug.Log ("FT - CheckAdAvailable...");
#if !UNITY_WP8
		FuseAPI.CheckAdAvailable();
#endif
#if UNITY_WP8
		WinPhoneAdMob.loadInterstitial (wpAdMob, false);
#endif
		yield return null;
	}
	
	private void AdAvailabilityResponse(bool IsAdAvailable, FuseAPI.AdAvailabilityError error)
	{
		Debug.Log ("FT - AdAvailabilityResponse - " + IsAdAvailable);

		isAdAvailable = IsAdAvailable;

		if (IsAdAvailable)
		{
			//Debug.Log ("FT - ShowAd");
			//FuseAPI.ShowAd();
		}
		else
		{
			Debug.Log("FT - AdAvailabilityResponse error: "+ error.ToString());
		}
	}

	private void SessionLoginError(FuseAPI.SessionError error)
	{
		print ("FT - SessionLoginError: " + error.ToString());
	}

	private void AdWillClose()
	{
		print ("FT - AdWillClose");
	}

	private void RewardedVideoCompleted(string adZone) {

		print ("FT - OnRewardedVideoCompleted - " + adZone);

		GameObject[] gos = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
		foreach (GameObject go in gos) {
			go.SendMessage("OnReward", SendMessageOptions.DontRequireReceiver);
		}
	}


/*	public void PreloadRewardedAd() {

		print ("FT - PreloadAd*");
		FuseAPI.PreLoadAd (rewardedZone);
	}
*/
	public void CheckForRewardedAd() {

		print ("FT - CheckForAd*");
#if UNITY_ANDROID
		FuseAPI.CheckAdAvailable(androidRewardedZone);
#endif
#if UNITY_IPHONE
		FuseAPI.CheckAdAvailable(iOSRewardedZone);
#endif


	}

	public void ShowRewardedAd() {

		print ("FT - ShowAd*");
#if UNITY_ANDROID
		FuseAPI.ShowAd (androidRewardedZone);
#endif
#if UNITY_IPHONE
		FuseAPI.ShowAd (iOSRewardedZone);
#endif
	}

/*	public void PreloadAd() {

		if (!isInitDone) return;

		print ("FT - PreloadAd");
		FuseAPI.PreLoadAd ();
	}
*/
	public void CheckForAd() {

		if (!isInitDone) return;

		print ("FT - CheckForAd");
		//StartCoroutine(WaitAndCheckAd());

#if UNITY_WP8
		WinPhoneAdMob.loadInterstitial (wpAdMob, false);
#else
		FuseAPI.CheckAdAvailable();
#endif

	}

	public void ShowAd() {

		if (!isInitDone) return;

		print ("FT - ShowAd");
		if (isAdAvailable) {
			isAdAvailable = false;
#if UNITY_WP8
			WinPhoneAdMob.showInterstitial();
#else
			FuseAPI.ShowAd ();
#endif
		}
		else {
			print ("FT - ShowAd, but had to cache");
#if UNITY_WP8
			WinPhoneAdMob.loadInterstitial(wpAdMob, false);
#else
			FuseAPI.CheckAdAvailable();
#endif
		}
	}

	public string GetSetting(string name, string defaultValue) {
		
		string sret = GetSetting (name);
		if (string.IsNullOrEmpty(sret)) sret = defaultValue;

		return sret;
	}


	public string GetSetting(string name) {

		if (_settings == null) {
			_settings = FuseAPI.GetGameConfig ();
		}

		if (_settings != null) {
			return _settings[name];
		}

		return null;
	}

	public void ShowGetMoreGames() {
		
		string url = "";
		#if UNITY_ANDROID
		url = GetLunaConfig ("sf-gmg-link", "https://play.google.com/store/apps/developer?id=Lunagames+Fun+%26+Games");
		#endif
		#if UNITY_IPHONE
		url = GetLunaConfig ("sf-gmg-link", "https://itunes.apple.com/app/id896202433");	// Crash & Burn
		#endif
		#if UNITY_WP8
		url = GetLunaConfig ("sf-gmg-link", "http://www.windowsphone.com/en-US/store/publishers?publisherId=Lunagames%2BInternational");
		#endif
		
		Application.OpenURL (url);
	}
	


	/// LUNASTREAMS 
	/// 
	/// 
	/// 
	private IEnumerator InitLunastreams(string lid)
	{
		// track boot count
		int bc = PlayerPrefs.GetInt (C_BOOTCOUNT, 0);
		bc++;
		PlayerPrefs.SetInt (C_BOOTCOUNT, bc);
		
		initUidAndInstallSettings();
		
		string url = getOnlineSettingsUrl();
		string onlineData = null;
		WWW www = new WWW(url);
		yield return www;
		if (www.isDone) onlineData = www.text;
		else onlineData = null;
		
		initOnlineSettings(onlineData);
		
		yield return null;
	}

	private static string GetLunaConfig(string name, string def) {
		
		return PlayerPrefs.GetString(name, def);
	}

	private static int GetBootCount() {
		
		return PlayerPrefs.GetInt (C_BOOTCOUNT, 1);
	}

	private string userUID;
	private const string C_BOOTCOUNT = "sf.bootcnt";
	private const string C_INSTALLDATE = "sf.installdate";
	private const string C_UID = "sf.uid";

	private void initUidAndInstallSettings() {
		
		string installDate = PlayerPrefs.GetString(C_INSTALLDATE, "");
		userUID = PlayerPrefs.GetString(C_UID, "");
		if (installDate.Length == 0) {
			
			//isFirstTimeStart = true;
			installDate = "" + GetTimeMillis()/1000;
			PlayerPrefs.SetString(C_INSTALLDATE, installDate);
			
			//gen Unique User Id
			userUID = installDate + "x" + UnityEngine.Random.Range(1000, 1000000) + "x" + UnityEngine.Random.Range(0, 10000);
			PlayerPrefs.SetString(C_UID, userUID);
			//Debug.Log ("UID created: " + userUID);
		}
	}
	
	private Int64 GetTimeMillis()
	{
		Int64 retval=0;
		var  st=  new DateTime(1970,1,1);
		TimeSpan t= (DateTime.Now.ToUniversalTime()-st);
		retval= (Int64)(t.TotalMilliseconds+0.5);
		return retval;
	}
	
	
	private string getOnlineSettingsUrl() {
		
		if (string.IsNullOrEmpty(lunastreamsID)) return null;
		
		/*
		app = unique app id
		bld = build (Nokia5800, iphone, Android, ...)
		ad = activation date. datum dat de app voor het eerst in gestart (dient client lokaal op te slaan)
		uid = unique user id
		w = width client device screen
		h = height client device screen
		v = app version
		*/
		string url = "http://adserver.lunastreams.com/luna/ads/?a=200&app=" + lunastreamsID;
		#if UNITY_ANDROID
		url += "&bld=android";
		url += "&store=google";
		/*		else if (androidStore == AndroidStore.Amazon) {
			url += "&store=amazon";
		} else if (androidStore == AndroidStore.NokiaStore) {
			url += "&store=nokiastore";
		}
*/
		#endif
		#if UNITY_IPHONE
		url += "&bld=iphone";
		url += "&store=apple";
		#endif
		#if UNITY_WP8
		url += "&bld=wp";
		url += "&store=wpstore";
		#endif
		#if UNITY_METRO
		url += "&bld=win8";
		url += "&store=winstore";
		#endif
		url += "&uid=" + userUID + "&ad=" + PlayerPrefs.GetString(C_INSTALLDATE, "") + "&v=1";
		url += "&w=" + Screen.width + "&h=" + Screen.height;
		url += "&bc=" + GetBootCount ();
		url += "&dui=" + WWW.EscapeURL(SystemInfo.deviceUniqueIdentifier);
		url += "&dm=" + WWW.EscapeURL(SystemInfo.deviceModel);
		url += "&dn=" + WWW.EscapeURL(SystemInfo.deviceName);

		Debug.Log ("Online settings: " + url);
		
		return url;
	}
	
	private void initOnlineSettings(string dataIfOnline) 
	{
		XDocument unityConfig = null;
		StreamReader reader;
		
		//Debug.Log("ONLINE SETTINGS!!!");
		
		//if(onlineSettings)
		{
			string result = dataIfOnline;
			
			if(result == null || result.Length == 0)
			{
				Debug.LogWarning("Online settings not available or time-out on request!");
				
				// try getting settings from cache
				result = PlayerPrefs.GetString("sf.onlinesettings", "");
				//Debug.Log("Result: " + result);
				if (result != null && result.Length > 0) 
				{
					unityConfig = XDocument.Parse(result);					
				}
				else 
				{
					unityConfig = null;	
				}
			}
			else
			{
				bool isValid = false;
				try 
				{
					unityConfig = XDocument.Parse(result);
					isValid = unityConfig.Root.Name.ToString().Equals("settings");
				}
				catch (Exception) 
				{
					isValid = false;
				}
				if (isValid) 
				{
					//gotOnlineSettings = true;
					//onlineSettingsDoc = unityConfig;
					
					// valid settings file, cache it
					PlayerPrefs.SetString("sf.onlinesettings", result);
				}
				else 
				{
					Debug.LogWarning("Online settings file was invalid!");
					// try getting settings from cache
					result = PlayerPrefs.GetString("sf.onlinesettings", "");
					if (result != null && result.Length > 0) 
					{
						unityConfig = XDocument.Parse(result);
					}
					else 
					{
						unityConfig = null;	
					}
				}
			}
			
			if (unityConfig != null) 
			{				
				// ad placement online settings
				//XmlNode unityNode = unityConfig.DocumentElement.SelectSingleNode("//settings/sparrow/config");
				
				
				
				XElement unityNode = unityConfig.Root.Element("sparrow");
				
				if(unityNode != null)
				{
					unityNode = unityNode.Element("config");
				}
				
				if (unityNode!=null) 
				{
					IEnumerable<XElement> aplacements = unityNode.Elements("prop");
					
					if (aplacements != null) 
					{
						foreach (XElement ap in aplacements)
						{
							if (ap.Attributes() != null)
							{
								XAttribute nameAttribute = ap.Attribute("name");
								XAttribute valAttribute = ap.Attribute("value");
								if (nameAttribute != null && valAttribute != null)
								{
									PlayerPrefs.SetString(nameAttribute.Value, valAttribute.Value);
									Debug.Log (nameAttribute.Value);
									//SfLibUserData.SetConfig(nameAttribute.Value, valAttribute.Value);
								}
							}
						}
					}
				}
			}
		}
	}


}

