﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UnlockScript : MonoBehaviour {

	// Use this for initialization
	public GameObject endlessModeLocked;

	// Use this for initialization
	void Start () {

		UnityEngine.UI.Button b = GetComponent<UnityEngine.UI.Button>();
		LevelUI lu = b.GetComponent<LevelUI>();

		if (lu.levelNumber == 15) {
			endlessModeLocked.SetActive(true);
			lu.showWhenLocked.SetActive(false);
			lu.showWhenUnLocked.SetActive(false);
			
		}

		if (!LevelManager.IsLocked (10)) {

			//Debug.Log ("Hello");
			if (lu.levelNumber == 15) {
				//Debug.Log ("nenujw");
				endlessModeLocked.SetActive(false);
				b.enabled = true;	// disable button when locked!

			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
