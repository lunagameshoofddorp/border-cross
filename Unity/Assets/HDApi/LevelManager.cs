﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {


	public static int GetUnlocked() {

		return PlayerPrefs.GetInt ("gamedata.level", 0);	// zero based level 0 is unlocked by default!
	}

	public static bool IsLocked(int level) {

		return level > GetUnlocked ();
	}

	public static void UnlockLevel(int levelUnlocked) {

		int current = GetUnlocked ();
		if (levelUnlocked > current) {

			// only store if actually unlocked greater level
			PlayerPrefs.SetInt("gamedata.level", levelUnlocked);

#if !UNITY_WP8
			FuseAPI.RegisterLevel(levelUnlocked);
#endif
		}
	}

}
