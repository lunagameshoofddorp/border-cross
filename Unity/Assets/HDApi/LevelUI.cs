﻿using UnityEngine;
using System.Collections;

public class LevelUI : MonoBehaviour {
	
	public int levelNumber = 0;
	public GameObject showWhenLocked;
	public GameObject showWhenUnLocked;
	public GameObject newUnit;

	
	// Use this for initialization
	void Start () {
		
		if (showWhenLocked) showWhenLocked.SetActive(LevelManager.IsLocked (levelNumber));
		if (showWhenUnLocked) showWhenUnLocked.SetActive(!LevelManager.IsLocked (levelNumber + 1 ));
		
		if (LevelManager.IsLocked (levelNumber)) {
			
			UnityEngine.UI.Button b = GetComponent<UnityEngine.UI.Button>();
			if (b!=null) {
				b.enabled = false;	// disable button when locked!
			}
		}

		if(levelNumber == 1 ){
			newUnit.SetActive(true);
		}

		if(levelNumber == 4 ){
			newUnit.SetActive(true);
		}

		if(levelNumber == 12 ){
			newUnit.SetActive(true);
		}

		if(levelNumber == 15 ){
			showWhenUnLocked.SetActive(false);
		}
	}
}
