using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FuseAPI_Stub : FuseAPI
{

#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_EDITOR

	public static bool debugOutput = false;

	#region Session Creation

	protected override void Init()
	{
		if(logging)
		{
			debugOutput = true;
		}
	}

	[Obsolete("StartSession now called automatically. Set gameID in the FuseAPI Component.", true)]
	new public static void StartSession(string gameId)
	{
	}

	new protected static void _StartSession(string gameId)
	{
		if(string.IsNullOrEmpty(gameId))
			Debug.LogError("FuseSDK: Null or empty API Key. Make sure your API Key is entered in the FuseSDK prefab");
	}

	#endregion
	
	#region Analytics Event
	
	new public static void RegisterEvent(string message)
	{
	}
	
	new public static void RegisterEvent(string message, Hashtable values)
	{
	}
	
	new public static int RegisterEvent(string name, string paramName, string paramValue, Hashtable variables)
	{
		return 0;
	}
	
	new public static int RegisterEvent(string name, string paramName, string paramValue, string variableName, double variableValue)
	{
		return 0;
	}
	
	#endregion
	
	#region In-App Purchase Logging

	
	new public static void RegisterInAppPurchaseList(Product[] products)
	{
	}
	

	new public static void RegisterInAppPurchase(string productId, string transactionId, byte[] transactionReceipt, TransactionState transactionState)
	{

	}
	
	new public static void RegisterUnibillPurchase(string productID, byte[] receipt)
	{

	}

	#endregion
	
	#region Fuse Ads

	new public static void PreLoadAd(string adZone)
	{

	}
	
	new public static void CheckAdAvailable(string adZone)
	{

	}
	
	new public static void ShowAd(string adZone)
	{

	}

	#endregion
	
	#region Notifications
	
	new public static void FuseAPI_RegisterForPushNotifications()
	{
	}
	
	new public static void DisplayNotifications()
	{
	}
	
	new public static bool IsNotificationAvailable()
	{
		return false;
	}

	#endregion
	
	#region More Games
	
	new public static void DisplayMoreGames()
	{

	}

	#endregion
	
	#region Gender
	
	new public static void RegisterGender(Gender gender)
	{
	}
	#endregion
	
	#region Account Login
	
	new public static void GameCenterLogin()
	{
	}
	
	new public static void FacebookLogin(string facebookId, string name, string accessToken)
	{
	}
	
	new public static void FacebookLogin(string facebookId, string name, Gender gender, string accessToken)
	{
	}
	
	new public static void TwitterLogin(string twitterId)
	{
	}
	
	new public static void DeviceLogin(string alias)
	{
	}

	new public static void FuseLogin(string fuseId, string alias)
	{
	}
	
	new public static void GooglePlayLogin(string alias, string token)
	{
	}
	
	new public static string GetOriginalAccountAlias()
	{
		return "Fuse_Account_Alias";
	}
	
	new public static string GetOriginalAccountId()
	{
		return "Fuse_Account_ID";
	}
	
	new public static AccountType GetOriginalAccountType()
	{
		return 0;
	}
	
	#endregion
	
	#region Miscellaneous
	
	new public static int GamesPlayed()
	{
		return 0;
	}
	
	new public static string LibraryVersion()
	{

		return "0.00";
	}
	
	new public static bool Connected()
	{
		return false;
	}
	
	new public static void TimeFromServer()
	{

	}
	
	new public static bool NotReadyToTerminate()
	{
		return false;
	}

	new public static void FuseLog(string str)
	{
		if(debugOutput)
		{
			Debug.Log("FuseAPI: " + str);
		}
	}
	
	new public static string GetFuseId()
	{
		return "00000000";
	}

	#endregion
	
	#region Data Opt In/Out
	
	new public static void EnableData(bool enable)
	{

	}
	
	new public static bool DataEnabled()
	{
		return false;
	}
	#endregion
	
	#region Friend List

	
	new public static void AddFriend(string fuseId)
	{
	}
	new public static void RemoveFriend(string fuseId)
	{
	}
	new public static void AcceptFriend(string fuseId)
	{
	}
	new public static void RejectFriend(string fuseId)
	{
	}	

	
	new public static void MigrateFriends(string fuseId)
	{
	}
	
	new public static void UpdateFriendsListFromServer()
	{
	}

	
	public static readonly List<Friend>  emptyFriendList;

	new public static List<Friend> GetFriendsList()
	{
		return emptyFriendList;
	}
	#endregion
	
	#region Chat List
	#endregion
	
	#region User-to-User Push Notifications
	new public static void UserPushNotification(string fuseId, string message)
	{
	}
	
	new public static void FriendsPushNotification(string message)
	{

	}
	#endregion
	
	#region Game Configuration Data
	
	new public static string GetGameConfigurationValue(string key)
	{
		return "";
	}

	new public static Dictionary<string, string> GetGameConfig()
	{
		return null;
	}
	
	#endregion
	
	#region Specific Event Registration
	new public static void RegisterLevel(int level)
	{
	}
	
	new public static void RegisterCurrency(int type, int balance)
	{
	}

	new public static void RegisterAge(int age)
	{

	}
	
	new public static void RegisterBirthday(int year, int month, int day)
	{

	}
	#endregion
	
	#region Internal Event Triggers
	new static protected void OnSessionStartReceived()
	{
	}
	
	new static protected void OnSessionLoginError(int error)
	{
	}
	
	new static protected void OnPurchaseVerification(int verified, string transactionId, string originalTransactionId)
	{
	}
	
	new static protected void OnAdAvailabilityResponse(int available, int error)
	{
	}
	
	new static protected void OnAdWillClose()
	{
	}
	
	new static protected void OnNotificationAction(string action)
	{
	}
	
	new static protected void OnOverlayWillClose()
	{
	}
	
	new static protected void OnAccountLoginComplete(AccountType type, string accountId)
	{
	}
	
	new static protected void OnTimeUpdated(DateTime time)
	{
	}
	
	new static protected void OnFriendAdded(string fuseId, int error)
	{
	}
	
	new static protected void OnFriendRemoved(string fuseId, int error)
	{
	}
	
	new static protected void OnFriendAccepted(string fuseId, int error)
	{
	}
	
	new static protected void OnFriendRejected(string fuseId, int error)
	{
	}
	
	new static protected void OnFriendsMigrated(string fuseId, int error)
	{
	}
	
	new static protected void OnFriendsListUpdated(List<Friend> friends)
	{
	}
	
	new static protected void OnFriendsListError(int error)
	{
	}
	
	new static protected void OnGameConfigurationReceived()
	{
	}
	#endregion
#endif//!UNITY_ANDROID && !UNITY_IPHONE && !UNITY_EDITOR
};